<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/16/2019
 * Time: 3:49 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class TagAsset extends AssetBundle
{

    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js'
    ];

    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}