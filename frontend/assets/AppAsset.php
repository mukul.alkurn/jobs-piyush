<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        'css/colors.css',
        'css/custom.css'
    ];
    public $js = [
      //  'js/jquery-migrate-3.1.0.min.js',
        'js/custom.js',
        'js/jquery.superfish.js',
        'js/jquery.themepunch.tools.min.js',
        'js/jquery.themepunch.revolution.min.js',
        'js/jquery.themepunch.showbizpro.min.js',
        'js/jquery.flexslider-min.js',
        'js/chosen.jquery.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/waypoints.min.js',
        'js/jquery.counterup.min.js',
        'js/jquery.jpanelmenu.js',
        'js/stacktable.js',
        'js/slick.min.js',
        //'js/headroom.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
