<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/16/2019
 * Time: 3:49 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class EditorAsset extends AssetBundle
{
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css',
    ];
    public $js = [
        'https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js',
    ];
}