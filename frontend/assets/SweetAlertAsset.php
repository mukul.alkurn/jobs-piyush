<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 17/11/17
 * Time: 10:55 AM
 */
namespace frontend\assets;

use yii\web\AssetBundle;

class SweetAlertAsset extends AssetBundle
{

    public $js = [
        'https://unpkg.com/sweetalert@2.0.8/dist/sweetalert.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
