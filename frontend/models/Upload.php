<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 12/18/2018
 * Time: 9:51 PM
 */

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class Upload extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $files;
    public $uploadedFiles = [];

    public function upload()
    {
        if($this->file) {

            $path = Yii::getAlias('@uploads') . '/' . $this->getBaseName();
            $base_name = preg_replace('/[^A-Za-z0-9\-]/', '', $this->file->baseName);
            $name = $base_name . '.' . $this->file->extension;
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $this->file->saveAs($path . $name);

            return $this->getBaseName() . $name;
        }
        return false;
    }

    public function uploadFiles()
    {
        if($this->files) {

            foreach ($this->files as $file) {
                $this->file = $file;
                $path = Yii::getAlias('@uploads') . '/' . $this->getBaseName();
                $base_name = preg_replace('/[^A-Za-z0-9\-]/', '', $this->file->baseName);
                $name = $base_name . '.' . $this->file->extension;
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $this->file->saveAs($path . $name);
                $this->uploadedFiles[] = $this->getBaseName() . $name;
            }

            return $this->uploadedFiles;
        }
        return false;
    }

    public function getBaseName()
    {
        return chunk_split(substr(preg_replace('/[^A-Za-z0-9\-]/', '', $this->file->baseName), 0, 8), 1, '/');
    }

    public function createBasePath($path)
    {
        return chunk_split(substr(preg_replace('/[^A-Za-z0-9\-]/', '', $path), 0, 8), 1, '/');
    }
}