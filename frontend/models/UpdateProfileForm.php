<?php
/**
 * Created by PhpStorm.
 * User: nomaan
 * Date: 23/1/18
 * Time: 9:42 PM
 */

namespace frontend\models;


use yii\base\Model;
use yii\web\UploadedFile;

class UpdateProfileForm extends Model
{
    public $username;

    public $emailAddress;

    public $firstName;

    public $lastName;

    public $about;

    public $mobile;

    public $address;

    public $city;

    public $state;

    public $country;

    public $latitude;

    public $longitude;

    public $placeId;

    public $postalCode;

    public $gender;
    public $age;
    public $experience;
    public $title;
    public $education_level;
    public $salary;
    public $estd_since;
    public $team_size;
    public $business_type;
    public $company_name;

    public $category;
    public $tehsil;
    public $district;
    public $dob;
    public $marital_status;


    /**
     * @var UploadedFile
     */
    public $avatar;

    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'string', 'max' => 64],
            [['address', 'city', 'state', 'country','tehsil','district', 'latitude', 'longitude', 'placeId', 'postalCode', 'about'], 'string'],
            ['mobile', 'integer'],
            [['dob','marital_status'],'required'],
            [['estd_since','team_size','business_type','company_name'],'string'],
            [['age', 'experience', 'title', 'education_level', 'address', 'city', 'state', 'salary','firstName', 'lastName', 'address','category'], 'required', 'on' => 'candidate-update'],
            [['company_name', 'business_type', 'team_size', 'estd_since', 'address', 'city', 'state','postalCode'], 'required', 'on' => 'company-update'],
            ['gender', 'in', 'range' => ['male', 'female']],
            ['avatar', 'image', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2, 'maxFiles' => 1]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Profile Title',
            'about' =>'About me (You can type also in Hinglish)',
            'city' =>'City/HomeTown',
            'tehsil' =>'Tehsil',
            //'age' => 'Age',
            //'education_level' => 'Education Level',
            //'education_level' => 'Education Level',
            // 'title' => 'Education Level',
        ];
    }
}