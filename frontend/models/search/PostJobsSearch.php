<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\PostJobs;
use Razorpay\Api;
/**
 * PostJobsSearch represents the model behind the search form of `backend\modules\user\models\PostJobs`.
 */
class PostJobsSearch extends PostJobs
{

    public $city ;
    public $state ;
    public $pincode ;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'posted_by','experience', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description', 'deadline'], 'safe'],
            [['offered_salary'], 'number'],
            [['industry','category'],'safe'],
            [['city','state','pincode'],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostJobs::find()
                ->joinWith(['postJobAddress']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'posted_by' => $this->posted_by,
//            'deadline' => $this->deadline,
//            'job_type' => $this->job_type,
//            'experience' => $this->experience,
//            'industry' => $this->industry,
//            'offered_salary' => $this->offered_salary,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'pincode', $this->pincode]);
        $query->andFilterWhere(['category'=>$this->category]);
        $query->andFilterWhere(['industry'=>$this->industry]);

        return $dataProvider;
    }
}
