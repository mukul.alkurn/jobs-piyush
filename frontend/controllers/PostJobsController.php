<?php

namespace frontend\controllers;

use backend\models\DynamicModel;
use backend\modules\user\models\ApplyJob;
use backend\modules\user\models\HrInfo;
use backend\modules\user\models\PostJobAddress;
use backend\modules\user\models\PostJobSpecailities;
use backend\modules\user\models\User;
use common\models\ChangePasswordForm;
use frontend\models\UpdateProfileForm;
use Mpdf\Tag\Hr;
use Yii;
use backend\modules\user\models\PostJobs;
use frontend\models\search\PostJobsSearch;
use yii\gii\components\DiffRendererHtmlInline;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostJobsController implements the CRUD actions for PostJobs model.
 */
class PostJobsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PostJobs models.
     * @return mixed
     */
    public function actionIndex()
    {
        /* @var $user User */
        $user = Yii::$app->user->identity;
        $dataProvider = $user->getJobs();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateCompanyProfile()
    {
        /* @var $user User */
        $user = Yii::$app->user->identity;
        $model = $this->loadUpdateProfile($user);
        $model->scenario = 'company-update';
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $profileData = [
                    'mobile' => $model->mobile,
                    'about' => $model->about,
                    'estd_since' => $model->estd_since,
                    'team_size' => $model->team_size,
                    'business_type' => $model->business_type,
                    'company_name' => $model->company_name,

                ];

                $user->userProfile->setAttributes($profileData);
                $user->userProfile->update(false, array_keys($profileData));

                $addressData = [
                    'address_line' => $model->address,
                    'city' => $model->city,
                    'state' => $model->state,
                    'country' => $model->country,
                    'latitude' => $model->latitude,
                    'longitude' => $model->longitude,
                    'postal_code' => $model->postalCode,
                ];

                $user->userAddress->setAttributes($addressData);
                $user->userAddress->update(false, array_keys($addressData));

                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Company info has been updated successfully.'
                ]);
            } else {
                //   pr($model->errors);
                Yii::$app->growl->setFlash([
                    'type' => 'info',
                    'message' => 'Error in saving company info.'
                ]);
            }
            return $this->redirect(['update-company-profile']);
        }

        return $this->render('update-company-profile', [
            'model' => $model,
        ]);

    }

    public function actionChangePassword()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return $this->redirect('/site/login');
        }
        /** @var $user User */
        $user = Yii::$app->user->identity;
        $model = new ChangePasswordForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->setPassword($_POST['ChangePasswordForm']['new_password']);
            $user->update();
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => 'Your password has been changed successfully.'
            ]);
            return $this->redirect(['update-company-profile']);
        }

        return $this->render('change-password', [
            'model' => $model
        ]);
    }

    protected function loadUpdateProfile(User $user)
    {
        $model = new UpdateProfileForm();
        $model->username = $user->username;
        $model->emailAddress = $user->email;
        $model->about = $user->userProfile->about;
        $model->team_size = $user->userProfile->team_size;
        $model->business_type = $user->userProfile->business_type;
        $model->estd_since = $user->userProfile->estd_since;
        $model->mobile = $user->userProfile->mobile;
        $model->address = $user->userAddress->address_line;
        $model->city = $user->userAddress->city;
        $model->state = $user->userAddress->state;
        $model->country = $user->userAddress->country;
        $model->latitude = $user->userAddress->latitude;
        $model->longitude = $user->userAddress->longitude;
        $model->postalCode = $user->userAddress->postal_code;
        $model->company_name = $user->userProfile->company_name;
        return $model;
    }

    /**
     * Displays a single PostJobs model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new PostJobsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new PostJobs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PostJobs();
        $modelAddress = new PostJobAddress();
        if ($model->load(Yii::$app->request->post()) &&
            $modelAddress->load(Yii::$app->request->post())
        ) {
            $model->posted_by = Yii::$app->user->id;
            if ($model->validate()) {
                if ($model->save(false)) {
                    $modelAddress->post_job_id = $model->id;
                    $modelAddress->save(false);

                    $special = $model->specialities;
                    foreach ($special as $item) {
                        $result = new PostJobSpecailities();
                        $result->post_job_id = $model->id;
                        $result->speciality_id = $item;
                        $result->save(false);
                    }

                    Yii::$app->growl->setFlash([
                        'type' => 'success',
                        'message' => 'Job has been posted successfully'
                    ]);
                } else {
                    Yii::$app->growl->setFlash([
                        'type' => 'success',
                        'message' => 'Error in posting Job'
                    ]);
                }
                return $this->redirect(['index']);
            } else {
                //pr($model->errors);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelAddress' => $modelAddress
        ]);
    }

    public function actionApply($id)
    {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post('user_id');
            $message = Yii::$app->request->post('message');
            $exist = ApplyJob::find()->where(['user_id' => $post, 'post_id' => $id])->exists();
            if ($exist) {
                return $this->asJson([
                    'flag' => false,
                    'message' => 'You have already applied for this job'
                ]);
            } else {
                $model = new ApplyJob();
                $model->user_id = $post;
                $model->post_id = $id;
                $model->messsage = $message;
                $model->save(false);
                return $this->asJson([
                    'flag' => true,
                    'message' => 'Job applied successful'
                ]);
            }

        }
    }

    public function actionHrInfo()
    {
        /** @var  $user  User */
        $user = Yii::$app->user->identity;
        $model = $user->hrInfos;

        if (Yii::$app->request->post()) {
            $oldIDs = ArrayHelper::map($model, 'id', 'id');
            $model = DynamicModel::createMultiple(HrInfo::classname(), $model);
            DynamicModel::loadMultiple($model, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model, 'id', 'id')));
            if (!empty($deletedIDs)) {
                HrInfo::deleteAll(['id' => $deletedIDs]);
            }
            foreach ($model as $modelBath) {
                $modelBath->user_id = Yii::$app->user->id;
                if (!($flag = $modelBath->save(false))) {
                    break;
                }
            }
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => Yii::t('app', 'Hr info saved successfully'),

            ]);
            return $this->redirect(['update-company-profile']);

        }
        return $this->render('hr-info', [

            'model' => (empty($model)) ? [new HrInfo()] : $model
        ]);
    }

    /**
     * Updates an existing PostJobs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAddress = $model->postJobAddress;
        $model->specialities = ArrayHelper::getColumn($model->postJobSpecailities, 'speciality_id');

        if ($model->load(Yii::$app->request->post()) &&
            $modelAddress->load(Yii::$app->request->post())
        ) {
            $model->posted_by = Yii::$app->user->id;
            if ($model->save(false)) {
                $modelAddress->post_job_id = $model->id;
                $modelAddress->save(false);

                $special = $model->specialities;
                if (!empty($special)) {
                    PostJobSpecailities::deleteAll(['post_job_id' => $model->id]);
                    foreach ($special as $item) {
                        $result = new PostJobSpecailities();
                        $result->post_job_id = $model->id;
                        $result->speciality_id = $item;
                        $result->save(false);
                    }
                }
                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Job has been updated successfully'
                ]);

            } else {
                Yii::$app->growl->setFlash([
                    'type' => 'warning',
                    'message' => 'Error in updating Job'
                ]);
            }
            return $this->redirect(['index']);

        }
        return $this->render('update', [
            'model' => $model,
            'modelAddress' => $modelAddress
        ]);
    }

    public function actionShow($id)
    {
        $model = $this->findModel($id);
        return $this->render('show', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing PostJobs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->growl->setFlash([
            'type' => 'success',
            'message' => 'Job has been deleted successfully'
        ]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the PostJobs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PostJobs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PostJobs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
