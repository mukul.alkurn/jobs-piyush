<?php

namespace frontend\controllers;

use backend\models\DynamicModel;
use backend\modules\user\models\GradSubName;
use backend\modules\user\models\PggradSubject;
use backend\modules\user\models\PggradSubName;
use backend\modules\user\models\PostJobs;
use backend\modules\user\models\User;
use backend\modules\user\models\User10;
use backend\modules\user\models\User12;
use backend\modules\user\models\UserAddress;
use backend\modules\user\models\UserCertficationName;
use backend\modules\user\models\UserCertification;
use backend\modules\user\models\UserDiploma;
use backend\modules\user\models\UserEducation;
use backend\modules\user\models\UserExperience;
use backend\modules\user\models\UserGrad;
use backend\modules\user\models\UserIti;
use backend\modules\user\models\UserJobCategory;
use backend\modules\user\models\UserLanguage;
use backend\modules\user\models\UserNotification;
use backend\modules\user\models\UserPgGrad;
use backend\modules\user\models\UserProfile;
use backend\modules\user\models\UserProfileApproval;
use backend\modules\user\models\UserProfileApprovalRequestStatus;
use backend\modules\user\models\UserProfileApprovalStatus;
use backend\modules\user\models\UserVerification;
use common\models\ChangePasswordForm;
use frontend\components\AuthHandler;
use frontend\models\search\PostJobsSearch;
use frontend\models\UpdateProfileForm;
use frontend\models\Upload;
use Razorpay\Api\Api;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main-home';
        $model = PostJobs::find()->limit(5)->all();
        $searchModel = new PostJobsSearch();
     //   $client = new Api('rzp_test_LIJ1ojNv6b23Bx', 'YxUpzCJKfrK3zGwv8HjLOi8X');
//        $order = $client->order->create([
//            'receipt' => 'order_rcptid_11',
//            'amount' => 497 * 100, // amount in the smallest currency unit
//            'currency' => 'INR',// <a href="https://razorpay.freshdesk.com/support/solutions/articles/11000065530-what-currencies-does-razorpay-support" target="_blank">See the list of supported currencies</a>.)
//            'payment_capture' => '1'
//        ]);
        return $this->render('index', [
            'model' => $model,
            'searchModel' =>$searchModel
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->can('employer')) {
                return $this->redirect(['/post-jobs/update-company-profile']);
            } elseif (Yii::$app->user->can('candidate')) {
                return $this->redirect(['/site/update-profile-candidate']);
            } else {
                return $this->redirect(['/site/index']);
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Thank you for contacting us. We will respond to you as soon as possible.'
                ]);
            } else {
                Yii::$app->growl->setFlash([
                    'type' => 'error',
                    'message' => 'There was an error sending your message.'
                ]);
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionCandidateSignup()
    {

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    $password = $model->password;

                    if ($user = $model->signup($role = '')) {

                        $backendUser = User::findOne($user->id);
                        $this->assignRole($user, 'candidate');
                        $this->saveProfile($user, $model);
                        $this->saveAddress($user, $model);
                        $this->saveNotification($user);
                        $userVerification = $this->saveUserVerification($user);
                        $html = 'email-html';
                        $text = 'email-text';

                        \Yii::$app->mailer->compose(
                            ['html' => $html, 'text' => $text],
                            [
                                'password' => $password,
                                'email' => $model->email,
                                'link' => Url::to(['/site/email-verification', 'token' => $userVerification->token
                                ], true)

                            ]
                        )
                            ->setFrom('info@jobscapital.in')
                            ->setTo($model->email)
                            ->setSubject('Please verify your account')
                            ->send();

                        $transaction->commit();
                        Yii::$app->growl->setFlash([
                            'type' => 'success',
                            'message' => 'Your account has been created successfully. Check your email for further instructions.'
                        ]);

                        return $this->redirect(['login']);

                    } else {

                        Yii::$app->growl->setFlash([
                            'type' => 'info',
                            'message' => 'Unable to create your account.'
                        ]);
                    }
                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    Yii::$app->growl->setFlash([
                        'type' => 'info',
                        'message' => 'An error occurred while processing your request. Please try again.'
                    ]);
                }
            }
        }

        return $this->render('candidate-signup', [
            'model' => $model,
        ]);
    }

    public function actionEmployerSignup()
    {
        $model = new SignupForm(['scenario' => 'employer']);
        if ($model->load(Yii::$app->request->post())) {
          //  $model->certificate = UploadedFile::getInstance($model, 'certificate');

            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    $password = $model->password;

                    if ($user = $model->signup($role = 'employer')) {

                        $backendUser = User::findOne($user->id);
                        $this->assignRole($user, 'employer');
                        $this->saveProfile($user, $model);
                        $this->saveAddress($user, $model);
                        $this->saveNotification($user);
                        $userVerification = $this->saveUserVerification($user);
                        $html = 'email-employer-html';
                        $text = 'email-employer-text';

                        \Yii::$app->mailer->compose(
                            ['html' => $html, 'text' => $text],
                            [
                                'password' => $password,
                                'email' => $model->email
                            ]
                        )
                            ->setFrom('info@jobscapital.in')
                            ->setTo($model->email)
                            ->setSubject('Registration done successfully')
                            ->send();

                        $transaction->commit();
                        Yii::$app->growl->setFlash([
                            'type' => 'success',
                            'message' => 'Your account has been created successfully. Check your email for further instructions.'
                        ]);

                        return $this->redirect(['login']);

                    } else {

                        Yii::$app->growl->setFlash([
                            'type' => 'info',
                            'message' => 'Unable to create your account.'
                        ]);
                    }
                } catch (\Exception $exception) {
                    // pr($exception->getMessage());
                    $transaction->rollBack();
                    Yii::$app->growl->setFlash([
                        'type' => 'info',
                        'message' => 'An error occurred while processing your request. Please try again.'
                    ]);
                }
            }
        }

        return $this->render('employer-signup', [
            'model' => $model,
        ]);
    }


    public function actionUpdateProfile()
    {
        /** @var $user User */
        $user = Yii::$app->user->identity;

        $model = $this->loadUpdateProfile($user);

        if ($model->load(Yii::$app->request->post())) {

            $model->avatar = UploadedFile::getInstance($model, 'avatar');

            if ($model->validate()) {

            }
        }

        return $this->render('update-profile', [
            'user' => $user,
            'model' => $model
        ]);
    }

    public function actionUpdateProfileCandidate()
    {
        /* @var $user User */
        $user = Yii::$app->user->identity;
        $model = $this->loadUpdateProfile($user);
        $model->scenario = 'candidate-update';
        if ($model->load(Yii::$app->request->post())) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            if ($model->validate()) {
                $profileData = [
                    'first_name' => $model->firstName,
                    'last_name' => $model->lastName,
                    'mobile' => $model->mobile,
                    'gender' => $model->gender,
                    'about' => $model->about,
                    'salary' => $model->salary,
                    'title' => $model->title,
                    'age' => $model->age,
                    'education_level' => $model->education_level,
                    'experience' => $model->experience,
                ];
                if (!empty($model->avatar)) {
                    $modelUpload = new Upload();
                    $modelUpload->file = $model->avatar;
                    $avatar = $modelUpload->upload();
                    if (is_string($avatar)) {
                        $profileData['avatar'] = $avatar;
                    }
                }
                $modelCategory = $model->category;
                foreach ($modelCategory as $result) {
                    $category = new UserJobCategory();
                    $category->user_id = $user->id;
                    $category->category_id = $result;
                    $category->save(false);
                }

                $user->userProfile->setAttributes($profileData);
                $user->userProfile->update(false, array_keys($profileData));

                $addressData = [
                    'address_line' => $model->address,
                    'city' => $model->city,
                    'state' => $model->state,
                    'country' => $model->country,
                    'tehsil' => $model->tehsil,
                    'district' => $model->district,
                    'latitude' => $model->latitude,
                    'longitude' => $model->longitude,
                    'postal_code' => $model->postalCode,
                ];

                $user->userAddress->setAttributes($addressData);
                $user->userAddress->update(false, array_keys($addressData));

                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Profile data has been updated successfully.'
                ]);
            }
        }

        return $this->render('update-profile-candidate', [
            'model' => $model,
        ]);

    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Check your email for further instructions.'
                ]);
                return $this->goHome();
            } else {
                Yii::$app->growl->setFlash([
                    'type' => 'error',
                    'message' => 'Sorry, we are unable to reset password for the provided email address.'
                ]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => 'New password saved.'
            ]);

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return $this->redirect('/site/login');
        }
        /** @var $user User */
        $user = Yii::$app->user->identity;
        $model = new ChangePasswordForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->setPassword($_POST['ChangePasswordForm']['new_password']);
            $user->update();
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => 'Your password has been changed successfully.'
            ]);
            return $this->redirect(['update-profile-candidate']);
        }

        return $this->render('change-password', [
            'model' => $model
        ]);
    }

    public function actionAddCertificate()
    {
        $model = new UserCertification();
        $name = new UserCertficationName();
        /** @var  $user  User */
        $user = Yii::$app->user->identity;
        $modelsEducation = $user->userCertficationNames;
        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsEducation, 'id', 'id');
            $modelsEducation = DynamicModel::createMultiple(UserCertficationName::classname(), $modelsEducation);
            DynamicModel::loadMultiple($modelsEducation, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsEducation, 'id', 'id')));
            if (!empty($deletedIDs)) {
                UserCertficationName::deleteAll(['id' => $deletedIDs]);
            }
            foreach ($modelsEducation as $modelBath) {
                $modelBath->user_id = Yii::$app->user->id;
                if (!($flag = $modelBath->save(false))) {
                    break;
                }
            }

            $file = UploadedFile::getInstances($model, 'file');
            if (!empty($file)) {
                UserCertification::deleteAll(['user_id' => Yii::$app->user->id]);
                $Upload = new Upload();
                $Upload->files = $file;
                $files = $Upload->uploadFiles();
                foreach ($files as $resume) {
                    $userfiles = new UserCertification;
                    $userfiles->file = $resume;
                    $userfiles->user_id = Yii::$app->user->id;
                    $userfiles->save(false);
                }
            }
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => Yii::t('app', 'Certificate saved  successfully'),

            ]);
            return $this->redirect(['update-profile-candidate']);
        }


        return $this->render('add-certificate', [
            'model' => $model,
            'name' => (empty($modelsEducation)) ? [new UserCertficationName()] : $modelsEducation

        ]);

    }

    public function actionAddResume()
    {
        $model = new UserProfile();
        if (Yii::$app->request->post()) {
            $file = UploadedFile::getInstance($model, 'resume');
            /* @var $user User */
            $user = Yii::$app->user->identity;
            $profileData = [];
            if (!empty($file)) {
                $modelUpload = new Upload();
                $modelUpload->file = $file;
                $avatar = $modelUpload->upload();
                if (is_string($avatar)) {
                    $profileData['resume'] = $avatar;
                }
                $user->userProfile->setAttributes($profileData);
                $user->userProfile->update(false, array_keys($profileData));
            }
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => Yii::t('app', 'Resume saved  successfully'),

            ]);
            return $this->redirect(['update-profile-candidate']);
        }
        return $this->render('add-resume', [
            'model' => $model
        ]);

    }

    public function actionAddLanguage()
    {
        /* @var $user User */
        $model = new UserLanguage();
        $user = Yii::$app->user->identity;
        $model->language = ArrayHelper::getColumn($user->userLanguages, 'language');
        $model->english_low = $user->userProfile->english_low;
        $model->english_medium = $user->userProfile->english_medium;
        $model->english_good = $user->userProfile->english_good;
        $model->english_vgood = $user->userProfile->english_vgood;
        $model->english_average = $user->userProfile->english_average;
        if ($model->load(Yii::$app->request->post())) {
            $language = $model->language;
            if (!empty($language)) {
                foreach ($language as $result) {
                    $out = new UserLanguage();
                    $out->user_id = $user->id;
                    $out->language = $result;
                    $out->save(false);
                }

            }
            $profileData = [
                'english_low' => $model->english_low,
                'english_average' => $model->english_average,
                'english_medium' => $model->english_medium,
                'english_good' => $model->english_good,
                'english_vgood' => $model->english_vgood,

            ];


            $user->userProfile->setAttributes($profileData);
            $user->userProfile->update(false, array_keys($profileData));
            // pr($user->userProfile);
            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => Yii::t('app', 'Language saved  successfully'),

            ]);
            return $this->redirect(['update-profile-candidate']);
        }
        return $this->render('add-language', [
            'model' => $model
        ]);

    }

    public
    function actionDelete()
    {
        $id = $_POST['key'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        UserCertification::deleteAll([
            'id' => $id
        ]);
        return true;
    }

    public function actionMarkFresher()
    {
        if (Yii::$app->request->post()) {
            $name = Yii::$app->request->post('fresher');
            /* @var $user User */
            $user = Yii::$app->user->identity;
            $user->userProfile->fresher = $name;
            $user->userProfile->save(false);
        }
    }

    /**
     * Email verification action
     *
     * @param $token
     * @return string|Response
     */
    public function actionEmailVerification($token)
    {
        $userVerification = $this->findUserVerificationByTokenModel($token);

        if ($userVerification) {
            if ($userVerification->responded == 0) {
                $userVerification->user->status = 1;
                $userVerification->user->update();
                $userVerification->responded = 1;
                $userVerification->update();

                /** @var $user User */
                $user = $userVerification->user;
                Yii::$app->user->loginByAccessToken($user->auth_key);


                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Your account has been verified successfully.'
                ]);

                return $this->redirect(['update-profile-candidate']);
            } else {

                Yii::$app->growl->setFlash([
                    'type' => 'info',
                    'message' => 'Your account has already been verified successfully.'
                ]);
                return $this->redirect(['login']);
            }
        } else {
            Yii::$app->growl->setFlash([
                'type' => 'info',
                'message' => 'Invalid verification token specified.'
            ]);
            return $this->redirect(['login']);
        }
    }

    public function actionLinkSocialAccounts()
    {
        /** @var $user User */
        $user = Yii::$app->user->identity;

        return $this->render('link-social-accounts');
    }

    /**
     * Resend the verification token
     *
     * @return \yii\web\Response
     */
    public function actionResendVerificationToken()
    {
        /** @var $user \backend\modules\user\models\User */
        $user = Yii::$app->user->identity;

        $modelUserVerification = new UserVerification();
        if (!$user->userVerification) {
            $modelUserVerification->token = $modelUserVerification->generateUniqueRandomString('token');
            $modelUserVerification->request_time = date('Y-m-d H:i:s');
            $modelUserVerification->save(false);
            $userVerification = $modelUserVerification;
        } else {
            $user->userVerification->token = $modelUserVerification->generateUniqueRandomString('token');
            $user->userVerification->request_time = date('Y-m-d H:i:s');
            $user->userVerification->update(false);
            $userVerification = $user->userVerification;
        }

        $userVerification = $user->userVerification;

        Yii::$app->emailtemplates->sendMail([
            $user->email => $user->userProfile->getName()
        ], 'email-verification', [
            'full_name' => $user->userProfile->getName(),
            'link' => Url::to(['/site/email-verification', 'token' => $userVerification->token], true)
        ]);

        Yii::$app->growl->setFlash([
            'type' => 'success',
            'message' => 'We have sent a new verification token. Please check your email for further instructions.'
        ]);

        return $this->redirect(['dashboard']);
    }

    public function actionSubmitForApproval()
    {
        /** @var $model User */
        $model = Yii::$app->user->identity;

        $modelApproval = new UserProfileApproval();
        $modelApproval->user_id = $model->id;
        $modelApproval->status = UserProfileApprovalRequestStatus::UNDER_REVIEW;
        $modelApproval->save(false);

        $model->userProfile->is_profile_approved = UserProfileApprovalStatus::UNDER_REVIEW;
        $model->userProfile->update(false);

        Yii::$app->emailtemplates->sendMail([
            $model->email => $model->userProfile->getName()
        ], 'new-profile-approval-request', [
            'full_name' => Yii::$app->settings->get('SiteConfiguration', 'appName', Yii::$app->name),
            'user_full_name' => $model->userProfile->getName(),
            'email' => $model->email,
            'role' => implode(', ', $model->getAssignedRoles()),
            'link' => Yii::$app->urlManagerBackEnd->createAbsoluteUrl(['/user/default/view', 'id' => $model->id], true),
        ]);

        Yii::$app->growl->setFlash([
            'type' => 'success',
            'message' => 'Your profile has been submitted for approval.'
        ]);

        return $this->redirect(['dashboard']);
    }

    /**
     * This action should respond to 'robots.txt' request.
     * It will create its content "on the fly", taking in account current environment.
     */
    public function actionRobots()
    {
        if (!Yii::$app->settings->get('SiteConfiguration', 'enableRobots'))
            return;
        $disallow = YII_ENV_PROD ? '' : "\nDisallow: /";
        $siteMapUrl = Yii::$app->urlManager->getHostInfo() . Yii::$app->urlManager->getBaseUrl() . '/sitemap/sitemap.xml';
        $content = <<<ROBOTS
User-agent: *{$disallow}
Sitemap: {$siteMapUrl}
ROBOTS;
        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_RAW;
        $response->getHeaders()->add('Content-Type', 'text/plain; charset=UTF-8');
        $response->content = $content;
        return $response;
    }

    protected function loadUpdateProfile(User $user)
    {
        $model = new UpdateProfileForm();
        $model->username = $user->username;
        $model->emailAddress = $user->email;
        $model->firstName = $user->userProfile->first_name;
        $model->lastName = $user->userProfile->last_name;
        $model->dob = $user->userProfile->dob;
        $model->about = $user->userProfile->about;
        $model->mobile = $user->userProfile->mobile;
        $model->gender = $user->userProfile->gender;
        $model->address = $user->userAddress->address_line;
        $model->city = $user->userAddress->city;
        $model->state = $user->userAddress->state;
        $model->country = $user->userAddress->country;
        $model->latitude = $user->userAddress->latitude;
        $model->longitude = $user->userAddress->longitude;
        $model->postalCode = $user->userAddress->postal_code;
        $model->age = $user->userProfile->age;
        $model->salary = $user->userProfile->salary;
        $model->experience = $user->userProfile->experience;
        $model->education_level = $user->userProfile->education_level;
        $model->title = $user->userProfile->title;
        $model->company_name = $user->userProfile->company_name;
        $model->marital_status = $user->userProfile->marital_status;
        $model->category = ArrayHelper::map($user->userJobCategories, 'id', 'category.name');
        return $model;
    }

    protected function assignRole($user, $role_name)
    {
        $manager = Yii::$app->authManager;
        $role = $manager->getRole($role_name);
        $role = $role ?: $manager->getPermission($role_name);
        $manager->assign($role, $user->id);
    }

    protected function saveProfile($user, $model)
    {
        $modelProfile = new UserProfile();
        $modelProfile->user_id = $user->id;
        $modelProfile->first_name = isset($model->first_name) ? $model->first_name : null;
        $modelProfile->last_name = isset($model->last_name) ? $model->last_name : null;
        $modelProfile->mobile = isset($model->mobile) ? $model->mobile : null;
        $modelProfile->orgnization_name = isset($model->organization_name) ? $model->organization_name : null;
        $temp_avatar = UploadedFile::getInstance($model, 'certificate');
        if ($temp_avatar !== NULL) {

            $upload = new Upload();
            $upload->file = $temp_avatar;
            $upload->file = $temp_avatar;
            $files = $upload->upload();
            $modelProfile->certificate = $files;

        }
        if ($modelProfile->save(false)) {
            return true;
        }
        throw new HttpException(405, 'Error saving model modelProfile');
    }


    public function actionAddEducation()
    {
        /* @var $user  User */
        $user = Yii::$app->user->identity;
        $model10 = new User10();
        //$model10->user_id = Yii::$app->user->id;
        $model10->board_10 = isset($user->user10->board_university) ? $user->user10->board_university : '';
        $model10->year_10 = isset($user->user10->year_of_passing) ? $user->user10->year_of_passing : '';
        $model10->per_10 = isset($user->user10->percentage) ? $user->user10->percentage : '';


        $model10->iti_name = isset($user->userIti->name) ? $user->userIti->name : '';
        $model10->iti_year = isset($user->userIti->year_of_passing) ? $user->userIti->year_of_passing : '';
        $model10->iti_per = isset($user->userIti->percentage) ? $user->userIti->percentage : '';
        $model10->iti_board = isset($user->userIti->board_university) ? $user->userIti->board_university : '';

    $model10->dip_name = isset($user->userDiploma->name) ? $user->userDiploma->name : '';
    $model10->dip_year = isset($user->userDiploma->year_of_passing) ? $user->userDiploma->year_of_passing : '';
     $model10->dip_per = isset($user->userDiploma->percentage) ? $user->userDiploma->percentage : '';
     $model10->dip_board = isset($user->userDiploma->board_university) ? $user->userDiploma->board_university : '';

        $model10->board_12 = isset($user->user12->board_university) ? $user->user12->board_university : '';
        $model10->year_12 = isset($user->user12->year_of_passing) ? $user->user12->year_of_passing : '';
        $model10->stream_12 = isset($user->user12->stream) ? $user->user12->stream : '';
        $model10->per_12 = isset($user->user12->percentage) ? $user->user12->percentage : '';


        $model10->branch_grad = isset($user->userGrad->branch) ? $user->userGrad->branch : '';
        $model10->board_grad = isset($user->userGrad->board_university) ? $user->userGrad->board_university : '';
        $model10->per_grad = isset($user->userGrad->percentage) ? $user->userGrad->percentage : '';
        $model10->year_grad = isset($user->userGrad->year_of_passing) ? $user->userGrad->year_of_passing : '';
        $model10->stream_grad = isset($user->userGrad->stream) ? $user->userGrad->stream : '';


        $model10->per_pggrad = isset($user->userPgGrad->percentage) ? $user->userPgGrad->percentage : '';
        $model10->board_pggrad = isset($user->userPgGrad->board_university) ? $user->userPgGrad->board_university : '';
        $model10->branch_pggrad = isset($user->userPgGrad->branch) ? $user->userPgGrad->branch : '';
        $model10->year_pggrad = isset($user->userPgGrad->year_of_passing) ? $user->userPgGrad->year_of_passing : '';
        $model10->stream_pggrad = isset($user->userPgGrad->stream) ? $user->userPgGrad->stream : '';
        if ($model10->load(Yii::$app->request->post())) {


            if (!empty($model10->board_10)) {
                $ten = isset($user->user10) ? $user->user10 : new User10;
                $ten->user_id = Yii::$app->user->getId();
                $ten->board_university = $model10->board_10;
                $ten->year_of_passing = $model10->year_10;
                $ten->percentage = $model10->per_10;
                $ten->save(false);
            }

            if (!empty($model10->iti_year)) {
                $teniti = isset($user->userIti) ? $user->userIti : new UserIti();
                $teniti->user_id = Yii::$app->user->id;
                $teniti->name = $model10->iti_name;
                $teniti->board_university = $model10->iti_board;
                $teniti->year_of_passing = $model10->iti_year;
                $teniti->percentage = $model10->iti_per;
                $teniti->save(false);
            }
            if (!empty($model10->dip_year)) {
                $tendip = isset($user->userDiploma) ? $user->userDiploma : new UserDiploma();
                $tendip->user_id = Yii::$app->user->id;
                $tendip->name = $model10->dip_name;
                $tendip->board_university = $model10->dip_board;
                $tendip->year_of_passing = $model10->dip_year;
                $tendip->percentage = $model10->dip_per;
                $tendip->save(false);

            }

            if (!empty($model10->year_12)) {
                $ten12 = isset($user->user12) ? $user->user12 : new User12();
                $ten12->user_id = Yii::$app->user->id;
                $ten12->board_university = $model10->board_12;
                $ten12->percentage = $model10->per_12;
                $ten12->year_of_passing = $model10->year_12;
                $ten12->stream = $model10->stream_12;
                $ten12->save(false);
            }

            if (!empty($model10->year_grad)) {
                $tengrad = isset($user->userGrad) ? $user->userGrad : new UserGrad();
                $tengrad->user_id = Yii::$app->user->id;
                $tengrad->branch = $model10->branch_grad;
                $tengrad->board_university = $model10->board_grad;
                $tengrad->percentage = $model10->per_grad;
                $tengrad->year_of_passing = $model10->year_grad;
                $tengrad->stream = $model10->stream_grad;
                $tengrad->save(false);
            }

            if (!empty($model10->year_pggrad)) {
                $tengrad = isset($user->userPgGrad) ? $user->userPgGrad : new UserPgGrad();
                $tengrad->user_id = Yii::$app->user->id;
                $tengrad->branch = $model10->branch_pggrad;
                $tengrad->board_university = $model10->board_pggrad;
                $tengrad->percentage = $model10->per_pggrad;
                $tengrad->year_of_passing = $model10->year_pggrad;
                $tengrad->stream = $model10->stream_pggrad;
                $tengrad->save(false);
            }

            Yii::$app->growl->setFlash([
                'type' => 'success',
                'message' => 'Education saved successfully.'
            ]);
            return $this->redirect(['update-profile-candidate']);
        }
        return $this->render('add-education', [
            'model10' => $model10,
        ]);
    }

    public function actionGetSubCategory()//used for drop down
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $id = $parents[0];
                $models = GradSubName::find()
                    ->leftJoin('grad_subject', 'grad_subject.id = grad_sub_name.grad_id')
                    ->where(['grad_id' => $id])
                    ->all();
                $newData = array();
                foreach ($models as $model) {
                    $newData[] = array(
                        'id' => $model->id,
                        'name' => Yii::t('app', $model->name),
                    );
                }
                return Json::encode(['output' => $newData, 'selected' => '']);
            } else {
                return Json::encode(['output' => '', 'selected' => '']);
            }
        } else {
            return Json::encode(['output' => '', 'selected' => '']);
        }
    }

    public function actionGetSubCategoryp()//used for drop down
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $id = $parents[0];
                $models = PggradSubName::find()
                    ->leftJoin('pggrad_subject', 'pggrad_subject.id = pggrad_sub_name.pg_id')
                    ->where(['pg_id' => $id])
                    ->all();
                $newData = array();
                foreach ($models as $model) {
                    $newData[] = array(
                        'id' => $model->id,
                        'name' => Yii::t('app', $model->name),
                    );
                }
                return Json::encode(['output' => $newData, 'selected' => '']);
            } else {
                return Json::encode(['output' => '', 'selected' => '']);
            }
        } else {
            return Json::encode(['output' => '', 'selected' => '']);
        }
    }

    public function actionAddWorkExperience()
    {
        /* @var $user  User */
        $user = Yii::$app->user->identity;
        $modelsEducation = $user->userExperiences;
        if (Yii::$app->request->post()) {
            $oldIDs = ArrayHelper::map($modelsEducation, 'id', 'id');
            $modelsEducation = DynamicModel::createMultiple(UserExperience::classname(), $modelsEducation);
            DynamicModel::loadMultiple($modelsEducation, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsEducation, 'id', 'id')));
            $transaction = \Yii::$app->db->beginTransaction();
            try {

                if (!empty($deletedIDs)) {
                    UserExperience::deleteAll(['id' => $deletedIDs]);
                }
                foreach ($modelsEducation as $modelBath) {
                    $modelBath->user_id = Yii::$app->user->id;
                    if (!($flag = $modelBath->save(false))) {
                        $transaction->rollBack();
                        break;
                    }
                }
                $transaction->commit();
                Yii::$app->growl->setFlash([
                    'type' => 'success',
                    'message' => 'Experience saved successfully.'
                ]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->growl->setFlash([
                    'type' => 'info',
                    'message' => 'Error occurred  while saving Experience '
                ]);

            }
            return $this->redirect(['update-profile-candidate']);
        }
        return $this->render('add-experience', [
            'modelsEducation' => (empty($modelsEducation)) ? [new UserExperience()] : $modelsEducation
        ]);
    }

    protected function saveAddress($user, $model)
    {
        $modelAddress = new UserAddress();
        $modelAddress->user_id = $user->id;
        if (isset($model->zip_code)) {
            $modelAddress->postal_code = $model->zip_code;
        }

        if ($modelAddress->save(false)) {
            return true;
        }
        throw new HttpException(405, 'Error saving model modelAddress');
    }

    public function saveNotification($user)
    {
        $modelNotification = new UserNotification();
        $modelNotification->user_id = $user->id;

        if ($modelNotification->save(false)) {
            return true;
        }
        throw new HttpException(405, 'Error saving model modelNotification');
    }

    public function saveUserVerification($user)
    {
        $modelUserVerification = new UserVerification();
        $modelUserVerification->user_id = $user->id;
        $modelUserVerification->token = $modelUserVerification->generateUniqueRandomString('token');
        $modelUserVerification->request_time = date('Y-m-d H:i:s');
        if ($modelUserVerification->save(false)) {
            return $modelUserVerification;
        }
        throw new HttpException(405, 'Error saving model modelUserVerification');
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $token
     * @return bool | UserVerification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findUserVerificationByTokenModel($token)
    {
        if (($model = UserVerification::findOne(['token' => $token])) !== null) {
            return $model;
        } else {
            return false;
        }
    }
}
