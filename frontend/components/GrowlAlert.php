<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 11/11/2019
 * Time: 11:11 AM
 */

namespace frontend\components;


use yii\base\Component;

class GrowlAlert extends Component
{
    public $key = 'growl-message';

    public function setFlash($message)
    {
        \Yii::$app->getSession()->setFlash($this->key, $message);
    }
}