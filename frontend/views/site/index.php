<?php

/* @var $this yii\web\View */


$this->title = 'Home';

use yii\helpers\ArrayHelper;
use backend\modules\user\models\JobCategories;
use yii\widgets\ActiveForm;

$category = ArrayHelper::map(JobCategories::find()->limit(12)->all(), 'id', 'name');
$category_all = ArrayHelper::map(JobCategories::find()->all(), 'id', 'name');
$this->registerJsFile("@web/js/city.js");

?>

<section class="popular-categories">
    <div class="container">
        <div class="sixteen columns">
            <h3 class="margin-bottom-20 margin-top-10">Popular Categories</h3>

            <!-- Popular Categories -->
            <div class="categories-boxes-container">

                <!-- Box -->
                <?php foreach ($category as $key => $value) { ?>
                    <a href="<?= \yii\helpers\Url::to(['/post-jobs/search', 'PostJobsSearch[category]' => $key]) ?>"
                       class="category-small-box">
                        <h4><?= $value ?></h4>
                        <span>32</span>
                    </a>
                <?php } ?>


            </div>

            <div class="clearfix"></div>
            <div class="margin-top-30"></div>

            <a href="<?= \yii\helpers\Url::to(['/post-jobs/search'], true) ?>" class="button centered">Browse All
                Categories</a>
            <div class="margin-bottom-55"></div>
        </div>
    </div>
</section>
<section class="row col-sm-12 flip-banner-overlay">
    <div class="home-counter-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 col-sm-12"></div>
                <div class="col-lg-8 col-md-10 col-sm-12">
                    <div class="row display-flex" style="align-items: center; justify-content: center;">
                        <h3 class="headline centered">Search Job</h3>

                        <?php $form = ActiveForm::begin(['action' => '/post-jobs/search', 'method' => 'get',]) ?>
                        <div class="col-lg-3 col-md-4 col-sm-12 text-center counter-box">
                            <?= $form->field($searchModel, 'state')->dropDownList([])->label(false) ?>
                        </div>

                        <div class="col-lg-3 col-md-4 col-sm-12 text-center counter-box">
                            <?= $form->field($searchModel, 'city')->dropDownList(['prompt'=>'Choose City'])->label(false) ?>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 text-center counter-box">
                            <?= $form->field($searchModel, 'category')->dropDownList($category_all, ['id' => 'category', 'prompt' => 'Search Job Category'])->label(false) ?>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 text-center counter-box">
                            <?= \yii\helpers\Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
                <div class="col-lg-2 col-md-1 col-sm-12"></div>
            </div>
        </div>
    </div>
</section>
<section class="how-it-works">
    <div class="container">

        <!-- Recent Jobs -->
        <div class="eleven columns">
            <div class="padding-right">
                <h3 class="margin-bottom-25">Recent Jobs Posted</h3>
                <div class="listings-container">
                    <?php
                    if (!empty($model)) {
                        foreach ($model as $key => $value) { ?>
                            <a href="<?= \yii\helpers\Url::to(['/post-jobs/show', 'id' => $value->id], true) ?>"
                               class="listing full-time">
                                <div class="listing-logo">
                                    <img src="images/step1.png" alt="">
                                </div>

                                <div class="listing-title">
                                    <h4><?= $value->title ?></h4>
                                    <p>
                                        <span style="padding-right:25px"><i
                                                    class="fa fa-briefcase"></i> <?= $value->category0->name ?></span>
                                        <span style="padding-right:25px"><i
                                                    class="fa fa-map-marker"></i> <?= $value->postJobAddress->city ?></span>
                                        <span style="padding-right:25px"><i
                                                    class="fa fa-money"></i> <?= $value->offered_salary ?></span>
                                    </p>
                                </div>
                            </a>
                        <?php }
                    } ?>

                    <div class="clearfix"></div>
                    <div class="margin-top-30"></div>

                    <a href="<?= \yii\helpers\Url::to(['/post-jobs/search'], true) ?>" class="button centered">Browse All
                        Jobs</a>
                    <div class="margin-bottom-55"></div>
                </div>
            </div>

        </div>

        <div class="five columns">
                <h3 class="margin-bottom-25">How It Work</h3>
                <div class="listings-container">

                    <!-- Listing -->
                    <a href="#" class="listing full-time">
                        <div class="listing-logo">
                            <img src="images/step1.png" alt="">
                        </div>
                        <div class="listing-title">
                            <p>POST YOUR JOB</p>
                        </div>
                    </a>

                    <!-- Listing -->
                    <a href="#" class="listing part-time">
                        <div class="listing-logo">
                            <img src="images/step1a.png" alt="">
                        </div>
                        <div class="listing-title">
                            <p>WE ADVERTISE YOUR JOB</p>
                        </div>
                    </a>

                    <!-- Listing -->
                    <a href="#" class="listing full-time">
                        <div class="listing-logo">
                            <img src="images/step2.png" alt="">
                        </div>
                        <div class="listing-title">
                            <p>CANDIDATES CALL YOU</p>
                        </div>
                    </a>

                    <!-- Listing -->
                    <a href="#" class="listing internship">
                        <div class="listing-logo">
                            <img src="images/step3.png" alt="">
                        </div>
                        <div class="listing-title">
                            <p>TAKE INTERVIEW & HIRE</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
</section>






    <!-- Info Section -->
    <div class="container">
        <div class="sixteen columns">
            <h3 class="headline centered">
                What Our Users Say 😍
                <span class="margin-top-25">We collect reviews from our users so you can get an honest opinion of what an experience with our website are really like!</span>
            </h3>
        </div>
    </div>
    <!-- Info Section / End -->

    <!-- Testimonials Carousel -->
    <div class="fullwidth-carousel-container margin-top-20">
        <div class="testimonial-carousel testimonials">

            <!-- Item -->
            <div class="fw-carousel-review">
                <div class="testimonial-box">
                    <div class="testimonial">Capitalize on low hanging fruit to identify a ballpark value added activity
                        to beta test. Override the digital divide with additional clickthroughs from DevOps.
                        Nanotechnology immersion along the information highway will close.
                    </div>
                </div>
                <div class="testimonial-author">
                    <img src="images/resumes-list-avatar-03.png" alt="">
                    <h4>Tom Baker <span>HR Specialist</span></h4>
                </div>
            </div>

            <!-- Item -->
            <div class="fw-carousel-review">
                <div class="testimonial-box">
                    <div class="testimonial">Bring to the table win-win survival strategies to ensure proactive
                        domination. At the end of the day, going forward, a new normal that has evolved from generation
                        is on the runway heading towards a streamlined cloud content.
                    </div>
                </div>
                <div class="testimonial-author">
                    <img src="images/resumes-list-avatar-02.png" alt="">
                    <h4>Jennie Smith <span>Jobseeker</span></h4>
                </div>
            </div>

            <!-- Item -->
            <div class="fw-carousel-review">
                <div class="testimonial-box">
                    <div class="testimonial">Leverage agile frameworks to provide a robust synopsis for high level
                        overviews. Iterative approaches to corporate strategy foster collaborative thinking to further
                        the overall value proposition. Organically grow the holistic world view.
                    </div>
                </div>
                <div class="testimonial-author">
                    <img src="images/resumes-list-avatar-01.png" alt="">
                    <h4>Jack Paden <span>Jobseeker</span></h4>
                </div>
            </div>

        </div>
    </div>
    <!-- Testimonials Carousel / End -->

</section>


<!-- Flip banner -->
<a href="#" class="flip-banner margin-bottom-55" data-background="images/all-categories-photo.jpg" data-color="#176fc9"
   data-color-opacity="0.93">
    <div class="flip-banner-content">
        <h2 class="flip-visible">Step inside and see for yourself!</h2>
        <h2 class="flip-hidden">Get Started <i class="fa fa-angle-right"></i></h2>
    </div>
</a>
<!-- Flip banner / End -->

<section class="teams">
    <div class="container ">
        <div class="row">
            <div class="section-fullwidth col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cs-heading">
                            <h1 style="color:#292929 !important; font-size: 20px !important; font-weight:  !important; letter-spacing: 1px !important; line-height: 26px !important; text-align:Center; font-style:normal;">
                                AGENCIES AND IN HOUSE TEAMS</h1>
                            <div class="heading-description"
                                 style="color: !important; text-align: Center; font-style:normal;">the world's leading
                                digital Leading employaers already using jobandtalen
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cs-clinets  ">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img src="images/client-logo-jobline-1.jpg" alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-2.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-3.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-4.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-5.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-6.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-7.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-8.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-9.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-10.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-12.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-11.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-13.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-14.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-15.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-16.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-17.jpg"
                                                alt=""></a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                    <a href=""><img
                                                src="http://jobcareer.chimpgroup.com/wp-content/uploads/client-logo-jobline-18.jpg"
                                                alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$js = <<<js
  print_state("postjobssearch-state")
  
  $("#postjobssearch-state").on('change',function() {
    print_city('postjobssearch-city', this.selectedIndex);
  })
js;
$this->registerJs($js);
?>

