<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/14/2019
 * Time: 4:52 PM
 */

use yii\helpers\Url;

?>
<div class="dashboard-nav">
    <div class="dashboard-nav-inner">

        <ul data-submenu-title="Start">
            <li><a href="<?= Url::to(['/site/update-profile-candidate']) ?>">Update Profile</a></li>
        </ul>

        <ul data-submenu-title="Account">
            <li><a href="<?= Url::to(['/site/add-education']) ?>">Add Education</a></li>
            <li><a href="<?= Url::to(['/site/add-work-experience']) ?>">Add Experience Details</a></li>
            <li><a href="<?= Url::to(['/site/change-password']) ?>">Change Password</a></li>
            <li><a href="<?= Url::to(['/site/add-certificate']) ?>">Add Certificate </a></li>
            <li><a href="<?= Url::to(['/site/add-resume']) ?>">Add Resume </a></li>
            <li><a href="<?= Url::to(['/site/add-language']) ?>">Add Language </a></li>
            <li><a href="<?= Url::to(['/site/logout']) ?>" data-method="post">Logout</a></li>
        </ul>

    </div>
</div>
