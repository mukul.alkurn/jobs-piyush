<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/14/2019
 * Time: 12:32 PM
 */

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\modules\user\models\ItiSubject;

/* @user $user User */

$user = Yii::$app->user->identity;
$education_level = ['below 10' => 'Below 10', '10(ssc)' => '10(SSC)', '12(HSC)' => '12(HSC)',
    'Certificate' => 'Certificate', 'Diploma' => 'Diploma',
    'associate-degree' => 'Associate Degree',
    'bachelor-degree' => 'Bachelor Degree',
    'postgraduate' => 'Post Gradudate', 'doctorate-degree' => 'Doctorate Degree'];

$stream = [
    'General Science' => 'General Science',
    'Maths Science' => 'Maths Science',
    'Biology' => 'Biology',
    'Commerce' => 'Commerce',
    'Arts' => 'Arts',
    'Others' => 'Others'
];

$iti = \yii\helpers\ArrayHelper::map(ItiSubject::find()->all(),'name','name');
$dip = \yii\helpers\ArrayHelper::map(\backend\modules\user\models\DiplomaSub::find()->all(),'name','name');
$grd_subject = \yii\helpers\ArrayHelper::map(\backend\modules\user\models\GradSubject::find()->all(),'id','name');
$pggrd_subject = \yii\helpers\ArrayHelper::map(\backend\modules\user\models\PggradSubject::find()->all(),'id','name');
?>
<style type="text/css">
            .form-group > .checkbox > label { font-weight: bold !important }
             .form-group > .control-label { font-weight: bold !important }

</style>

    <section class="dash-content">
        <div class="container">
            <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
            <?= $this->render('_sidebar_candidate') ?>
            <div class="dashboard-content">

                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
                        <div class="col-sm-12">
                            <div class="col-sm-3">

                            <?= $form->field($model10, 'ten')->checkbox(['value' => '10', 'class' => '10 edu', 'checked' => isset($user->user10) ? true : false])->label('10th') ?>
                                
                            </div>
                            <div class ="col-sm-3">
                                <?= $form->field($model10, 'tweleve')->checkbox(['value' => '12', 'class' => '12 edu', 'checked' => isset($user->user12) ? true : false])->label('12th') ?>
                            </div>
                            <div class ="col-sm-3">
                                <?= $form->field($model10, 'dip')->checkbox(['value' => 'dip', 'class' => 'dip edu', 'checked' => isset($user->userDiploma) ? true : false])->label('Diploma') ?>
                            </div>
                            <div class ="col-sm-3">
                                <?= $form->field($model10, 'iti')->checkbox(['value' => 'iti', 'class' => 'iti edu', 'checked' => isset($user->userIti) ? true : false])->label('ITI ') ?>
                            </div>
                            <div class ="col-sm-3">
                                 <?= $form->field($model10, 'grd')->checkbox(['value' => 'grd', 'class' => 'grd edu', 'checked' => isset($user->userGrad) ? true : false])->label('Graduation') ?>
                            </div>

                            
                            <div class="col-sm-3">
                                 
                            <?= $form->field($model10, 'pg')->checkbox(['value' => 'pg', 'class' => 'pg edu', 'checked' => isset($user->userPgGrad) ? true : false])->label('PostGraduation') ?>
                            </div>
                            
                          
                        </div>
                        <div class="row ed" id="10">
                            <p>10TH Class Education</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'board_10')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'year_10')->textInput(['type'=>'date']) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'per_10')->textInput() ?>
                            </div>

                        </div>
                        <div class="row ed" id="dip">
                            <p>Diploma Education</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'dip_name')->dropDownList($dip) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'dip_year')->textInput(['type'=>'date']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'dip_per')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'dip_board')->textInput() ?>
                            </div>

                        </div>
                        <div class="row ed" id="iti">
                            <p>ITI Education</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'iti_name')->dropDownList($iti) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'iti_year')->textInput(['type'=>'date']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'iti_per')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'iti_board')->textInput() ?>
                            </div>

                        </div>

                        <div class="row ed" id="12">
                            <p>12TH Class Education</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'board_12')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'year_12')->textInput(['type'=>'date']) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'per_12')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'stream_12')->dropDownList($stream) ?>
                            </div>

                        </div>

                        <div class="row ed" id="grd">
                            <p>Graduation</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'branch_grad')->dropDownList($grd_subject,['id' => 'category-dep']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'stream_grad')->widget(\kartik\depdrop\DepDrop::classname(), [
                                    'options' => ['id' => 'state-dep','prompt' => 'Subject'],
                                    'pluginOptions' => [
                                        'depends' => ['category-dep'],
                                        'url' => \yii\helpers\Url::to(['get-sub-category'],true)
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'board_grad')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'year_grad')->textInput(['type'=>'date']) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'per_grad')->textInput() ?>
                            </div>

                        </div>
                        <div class="row ed" id="pg">
                            <p>Post Graduation</p>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'branch_pggrad')->dropDownList($pggrd_subject,['id' => 'pcategory-dep']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'stream_pggrad')->widget(\kartik\depdrop\DepDrop::classname(), [
                                    'options' => ['id' => 'state-deps','prompt' => 'Subject'],
                                    'pluginOptions' => [
                                        'depends' => ['pcategory-dep'],
                                        'url' => \yii\helpers\Url::to(['get-sub-categoryp'],true)
                                    ],
                                ]); ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'board_pggrad')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'year_pggrad')->textInput(['type'=>'date']) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model10, 'per_pggrad')->textInput() ?>
                            </div>

                        </div>

                        <div class="form-group">
                            <?= \yii\helpers\Html::submitButton('Add ', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="form-group col-sm-12 text-center">
                        <div class="form-group col-sm-12 text-center">
                            <?= Html::a('Next', ['add-work-experience'], ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>


<?php

$js = <<<js
$("#10,#12,#iti,#grd,#pg,#dip").hide();

 var checkbox = $(".edu");
        checkbox.each(function() {
                if($(this).is(":checked")){
                    $("#"+$(this).val()).show();
                }
   });
        
   
  var ten = $(".10");
  ten.on('change',function() {
                if($(this).is(":checked")){
                      $("#10").show()
                }else{
                    $("#10").hide();
                }
   });
 var twe = $(".12");
  twe.on('change',function() {
                if($(this).is(":checked")){
                      $("#12").show()
                }else{
                    $("#12").hide();
                }
   });
  var iti = $(".iti");
  iti.on('change',function() {
                if($(this).is(":checked")){
                      $("#iti").show()
                }else{
                    $("#iti").hide();
                }
   });
  var dip = $(".dip");
  dip.on('change',function() {
                if($(this).is(":checked")){
                      $("#dip").show()
                }else{
                    $("#dip").hide();
                }
   });
  var grad = $(".grd");
  grad.on('change',function() {
                if($(this).is(":checked")){
                      $("#grd").show()
                }else{
                    $("#grd").hide();
                }
   });
   var pg = $(".pg");
  pg.on('change',function() {
                if($(this).is(":checked")){
                      $("#pg").show()
                }else{
                    $("#pg").hide();
                }
   });
  
js;
$this->registerJs($js);
?>