<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\{Html,ArrayHelper};
use yii\bootstrap\ActiveForm;
use backend\modules\user\models\JobCategories;
use kartik\file\FileInput;

$this->title = 'Employer Register';
$this->params['breadcrumbs'][] = $this->title;
$category = ArrayHelper::map(JobCategories::find()->all(),'id','name');
?>

<section class="login-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-12"></div>
            <div class="col-lg-6 col-md-8 col-sm-12">
                <div class="loginbox">
                <h1><?= Html::encode($this->title) ?></h1>
                <p class="sub-title">Please fill out the following fields to signup:</p>

                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'mobile') ?>
                    <?= $form->field($model, 'organization_name')->textInput() ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Register', ['class' => 'btn-blue btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                  <?php ActiveForm::end(); ?>
              </div>
            </div>
        </div>
    </div>
</section>
