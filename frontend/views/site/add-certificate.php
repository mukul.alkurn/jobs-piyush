<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
/*  @var $user \backend\modules\user\models\User */
$user = Yii::$app->user->identity;
$files =  $initialPreviewConfig = [];
if (isset($user->getUserCertifications()->one()->file)) {
    foreach ($user->userCertifications as $img) {
        $type = '/uploads/' . $img->file;
        $files[] = $type;
        $initialPreviewConfig[] = [
            'width' => '120px',
            'key' => $img->id
        ];
    }
}
?>
<style>
    .e{color:black ;font-weight: 100}
    .bootstrap-tagsinput {
        width: 100%
    }
            .form-group > .control-label { font-weight: bold !important }

</style>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
        <?= $this->render('_sidebar_candidate') ?>

        <div class="dashboard-content">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                           <h3 style="font-family: verdana" ><strong class="col-sm-12">Add Extra-curricular Certificate Name</strong></h3>
            
                    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
                    <div class="col-sm-12">
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $name[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'certificate_name',
                                'training_institute',
                                'location',
                                'year',
                            ],
                        ]); ?>

                        <div class="panel panel-default">
                            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i
                                        class="fa fa-plus"></i>
                               Add Extra-curricular  Certificate Details
                            </button>
                            <div class="panel-body container-items"><!-- widgetContainer -->
                                <?php foreach ($name as $index => $name): ?>
                                    <div class="item panel panel-default"><!-- widgetBody -->
                                        <div class="panel-heading">
                                            Certificate  Details
                                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs">
                                                <i
                                                        class="fa fa-minus"></i></button>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <?= $form->field($name, "[{$index}]certificate_name")->textInput(['maxlength' => true])->label("Certificate Name <span class='e'> (e.x MSCIT)") ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?= $form->field($name, "[{$index}]training_institute")->textInput(['maxlength' => true])->label("Institute Name <span class='e'> (e.x Lotus Computer)") ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?= $form->field($name, "[{$index}]location")->textInput()->label("Location <span class='e'> (e.x Nagpur)") ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?= $form->field($name, "[{$index}]year")->textInput() ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    </div>
                       <h3 style="font-family: verdana"><strong class="col-sm-12">Upload Certificate</strong></h3>
            
                    <div class="col-sm-12">
                        <?= $form->field($model, 'file[]')->widget(\kartik\file\FileInput::classname(), [
                            'options' => ['accept' => 'image/*', 'multiple' => true],
                            'pluginOptions' => [
                                'initialPreviewAsData' => true,
                                'initialPreview' => $files,
                                'showCaption' => false,
                                'initialPreviewConfig' => $initialPreviewConfig,
                                'showRemove' => false,
                                'showUpload' => false,
                                'showCancel' => false,
                                'uploadAsync' => false,
                                'browseClass' => 'btn btn-primary radius px-5 py-1 text-uppercase',
                                'maxFileSize' => 1500,
                                'showClose' => false,
                                'deleteUrl' =>'delete',
                                'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg"],
                                'overwriteInitial' => true,
                                'fileActionSettings' => [
                                    'showDrag' => false,
                                    'showRemove' => false,
                                    'showZoom' => false
                                ],
                                'previewSettings' => [
                                    'image' => [
                                        'width' => "auto",
                                        'height' => "100%",
                                        'max-width' => "100%",
                                        'max-height' => "100%"
                                    ]
                                ],
                                'browseLabel' => 'Extra-curricular Activity Certificate',
                                'browseIcon' => '<i class="fa fa-camera"></i>&nbsp;',
                            ]
                        ])->label(false); ?>
                    </div>


                    <div class="form-group col-sm-12">
                        <?= Html::submitButton('Add Certificate', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                    <div class="form-group col-sm-12 text-center">
                        <?= Html::a('Next', ['add-resume'], ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>

</section>

