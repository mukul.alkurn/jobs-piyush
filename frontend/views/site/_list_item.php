<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 12/17/2019
 * Time: 8:20 AM
 */
?>
<li>
    <div class="job-spotlight">
        <a href="#"><h4>Social Media: Advertising Coordinator <span class="part-time">Part-Time</span>
            </h4></a>
        <span><i class="fa fa-briefcase"></i> Mates</span>
        <span><i class="fa fa-map-marker"></i> New York</span>
        <span><i class="fa fa-money"></i> $20 / hour</span>
        <p>As advertising & content coordinator, you will support our social media team in
            producing high quality social content for a range of media channels.</p>
        <a href="job-page.html" class="button">Apply For This Job</a>
    </div>
</li>
