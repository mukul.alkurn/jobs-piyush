<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
\frontend\assets\Select2Asset::register($this);
$this->title = 'Add Language';
$this->params['breadcrumbs'][] = $this->title;
/*  @var $user \backend\modules\user\models\User */
$user = Yii::$app->user->identity;
$files = [];
$languages = [
    'Hindi' => 'Hindi',
    'Bengali' => 'Bengali',
    'Telugu' => 'Telugu',
    'Marathi' => 'Marathi',
    'Tamil' => 'Tamil',
    'Urdu' => 'Urdu',
    'Gujarati' => 'Gujarati',
    'Kannada' => 'Kannada',
    'Malayalam' => 'Malayalam',
    'Oriya' => 'Oriya',
    'Punjabi' => 'Punjabi',
    'Assamese' => 'Assamese',
    'Maithili' => 'Maithili',
    'Bhili/Bhilodi' => 'Bhili/Bhilodi',
    'Santali' => 'Santali',
    'Kashmiri' => 'Kashmiri',
    'Nepali' => 'Nepali',
    'Gondi' => 'Gondi',
    'Sindhi' => 'Sindhi',
    'Konkani' => 'Konkani',
    'Dogri' => 'Dogri',
    'Khandeshi' => 'Khandeshi',
    'Kurukh' => 'Kurukh',
    'Tulu' => 'Tulu',
    'Meitei/Manipuri' => 'Meitei/Manipuri',
    'Bodo' => 'Bodo',
    'Khasi' => 'Khasi',
    'Ho' => 'Ho',
    'Mundari' => 'Mundari',
    'Garo' => 'Garo',
    'Tripuri' => 'Tripuri',
];


?>
<style type="text/css">
            .form-group > .control-label { font-weight: bold !important }

</style>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
        <?= $this->render('_sidebar_candidate') ?>

        <div class="dashboard-content">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <?php $form = ActiveForm::begin(['id' => 'user']); ?>
                    <div class="col-sm-12">
                        <?= $form->field($model, "language")->dropDownList($languages,['multiple' => true])->label("Languages I can speak") ?>
                    </div>
                    <div class="col-sm-12">
                        <p>English Knowledge</p>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($model, "english_low")->checkbox()->label('Low (Only can read)') ?>
                        <?= $form->field($model, "english_average")->checkbox()->label('Average (Only writing)') ?>
                        <?= $form->field($model, "english_medium")->checkbox()->label('Medium (Writing+ speak average)') ?>
                        <?= $form->field($model, "english_good")->checkbox()->label('Good (Writing+ speak medium)') ?>
                        <?= $form->field($model, "english_vgood")->checkbox()->label('Very good(Writing+ speak very good)') ?>
                    </div>


                    <div class="form-group col-sm-12">
                        <?= Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>

</section>

<?php
$js =<<<js
 $('#userlanguage-language').select2();
js;
$this->registerJs($js)
?>