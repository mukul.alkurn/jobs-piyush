<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/14/2019
 * Time: 12:32 PM
 */

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\modules\user\models\Experience;
use frontend\assets\{Select2Asset, EditorAsset};
use kartik\file\FileInput;
Select2Asset::register($this);
EditorAsset::register($this);
$education_level = ['below 10' => 'Below 10', '10(ssc)' => '10(SSC)', '12(HSC)' => '12(HSC)',
    'Certificate' => 'Certificate', 'Diploma' => 'Diploma',
    'associate-degree' => 'Associate Degree',
    'bachelor-degree' => 'Bachelor Degree',
    'postgraduate' => 'Post Gradudate', 'doctorate-degree' => 'Doctorate Degree'];
$salary = [
    '5000---10,000' => '5000---10,000',
    '15,000---16,000' => '15,000---16,000',
    '16,000---20,000' => '16,000---20,000',
    '20,000---25,000' => '20,000---25,000',
    '25,000---30,000' => '25,000---30,000',
    '30,000---45,000' => '30,000---45,000',
    '45,000---50,000' => '45,000---50,000',
    '50,000---60,000' => '50,000---60,000',
    'more-than-60,000' => 'more-than-60,000'

];

$age = [
    '18-22' => '18 - 22 Years',
    '23-27' => '23 - 27 Years',
    '28-32' => '28 - 32 Years',
    '33-37' => '23 - 27 Years',
    '38 - 42' => '38 - 42 Years',
    '43-47' => '43 - 47 Years',
    '48-52' => '48 - 52 Years',
    '53 - 57' => '53 - 57 Years',
    '+57 Years' => 'Above 57 Years'
];
$experience = \yii\helpers\ArrayHelper::map(Experience::find()->all(), 'id', 'name');
$category = \yii\helpers\ArrayHelper::map(\backend\modules\user\models\JobCategories::find()->all(), 'id', 'name');
$this->registerJsFile("@web/js/city.js");
/** @var  $user  \backend\modules\user\models\User */
$user = Yii::$app->user->identity;
$files = [];
if (isset($user->userProfile->avatar)) {
    $type = '/uploads/' . $user->userProfile->avatar;
    $files[] = $type;
    $initialPreviewConfig[] = [
        'width' => '120px',
    ];
}
$defaultUrl = '/uploads/'.$user->userProfile->avatar;
?>
    <style>
        .e{color:black ;font-weight: 100}
        .file-footer-buttons {
            display: none
        }
        .krajee-default.file-preview-frame .kv-file-content {margin: 0 auto}
        .krajee-default.file-preview-frame {border: none; box-shadow: none;
            padding: 0px; float:none;}
        .krajee-default.file-preview-frame:not(.file-preview-error):hover {
            border: none;
            box-shadow: none;}
        .krajee-default .file-caption-info, .krajee-default .file-size-info {
            width: auto;
            height: auto;
        }
        .krajee-default.file-preview-frame {
            background-color: transparent;
        }
        .file-preview {
            background-color: transparent;
            border:none;padding-bottom: 0px;
        }
        .file-drop-zone{border: none;margin-bottom: 0px;}
        .file-input{text-align: center;}
        .remove-item, .add-item {margin-top: -10px;}
        .krajee-default .file-footer-caption { display: none;margin-bottom: 0px;}
        .krajee-default.file-preview-frame .file-thumbnail-footer {height: 0px;}
        .form-group > .control-label { font-weight: bold !important }
    </style>

    <section class="dash-content">
        <div class="container">
            <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
            <?= $this->render('_sidebar_candidate') ?>
            <!-- Navigation / End -->


            <!-- Content
            ================================================== -->
            <div class="dashboard-content">

                <div class="row">
                    <div class="col-lg-12 col-sm-12">

                        <div class="col-sm-12">
                            <h3 style="font-family: verdana"><strong>Basic Info</strong></h3>
                        </div>
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <div class="col-sm-12">


                            <?= $form->field($model, 'avatar')->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'defaultPreviewContent' =>  $defaultUrl ,
                                    'initialPreview' => $files,
                                    'initialPreviewAsData' => true,
                                    'showCaption' => false,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'showCancel' => false,
                                    'browseClass' => 'btn btn-danger radius px-lg-5 py-2 mx-lg-5 text-uppercase',
                                    'maxFileSize' => 1500,
                                    'showClose' => false,
                                    'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg"],
                                    'overwriteInitial' => true,
                                    'fileActionSettings' => [
                                        'showDrag' => false,
                                        'showRemove' => false,
                                        'showZoom' => false
                                    ],
                                    'previewSettings' => [
                                        'image' => [

                                            'max-width' => "100%",
                                            'max-height' => "100%"
                                        ]
                                    ],
                                    'previewTemplates' => [
                                        'image' => '<div class="file-preview-frame krajee-default" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}">' .
                                            '   <div class="kv-file-content">' .
                                            '       <img src="{data}" class="kv-preview-data file-preview-image img-fluid rounded-circle border" title="{caption}" alt="{caption}" {style}>' .
                                            '   </div>' .
                                            '   {footer}' .
                                            '</div>',
                                    ],
                                    'browseLabel' => 'Change Profile Photo',
                                    'browseIcon' => '<i class="fa fa-camera"></i>&nbsp;',
                                ]
                            ])->label(false); ?>
                        </div>
                        
                        <div class="col-sm-6">
                            <?= $form->field($model, 'firstName')->textInput(['autofocus' => true])->label("First Name <span class='e'>(Mukul)</span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'lastName')->textInput(['autofocus' => true])->label("Last Name <span class='e'>(Rathi)</span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '9999999999',
                            ]) ?>

                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'dob')->textInput(['type'=>'date']) ?>

                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($model, 'gender')->dropDownList(['male' => 'male', 'female' => 'female']) ?>
                        </div>
                        <div class="col-sm-6">

                        <?= $form->field($model, 'title')->textInput([''])->label("Title <span class='e'>( I am Teacher,Fresher)</span>") ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model, 'about')->textarea()->label("About <span class='e'>(I am Mukul Rathi, I graduated B.E
from Bhopal university and all....')</span>") ?>
                        </div>

                        <div class="col-sm-12">
                     <h3 style="font-family: verdana"><strong>Contact Information</strong></h3>
              
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'address')->textInput()->label("<span class='e'> Address (e.x Plot no 228 , Nagpur) </span>") ?>
                        </div>
                        <div class="col-sm-6">

                            <?= $form->field($model, 'state')->dropDownList(['value' => $model->state])->label("State <span class='e'>(e.x Maharashtra)") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'tehsil')->dropDownList([$model->city])->label("Tehsil <span class='e'>(e.x Nagpur) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'district')->textInput()->label("District <span class='e'>(e.x Savner) </span>") ?>
                        </div>
                        
                        <div class="col-sm-6">
                            <?= $form->field($model, 'city')->textInput()->label('City/Hometown')->label("City/Hometown <span class='e'>(e.x Savner) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'postalCode')->textInput()->label("PostalCode <span class='e'>e.x (440003) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'country')->textInput(['value' => 'India', 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-12">
                        <h3 style="font-family: verdana"><strong>Extra Info</strong></h3>
            
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'category')->dropDownList($category, ['id' => 'category', 'multiple' => true])->label("Job Category <span class='e'>(e.x  Computer , Electrical Engineer) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'age')->textInput(['type' => 'number','min'=>18,'max'=>60])->label("Age <span class='e'>(e.x 20) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'marital_status')->dropDownList(['single' => 'Single','married'=>'Married'])->label("Marital Status <span class='e'>(e.x Single) </span>") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'experience')->dropDownList($experience)->label("Total Experience <span class='e'> (e.x 1 Years )") ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'salary')->dropDownList($salary)->label('Expected Salary e.x 15,000---16,000') ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'education_level')->dropDownList($education_level)->label("Education Level <span class='e'>(Certificate)</span> ") ?>
                        </div>

                        <div class="form-group col-sm-12">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                        <div class="form-group col-sm-12 text-center">
                            <?= Html::a('Next', ['add-education'], ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php
$js = <<<js
 $('#category').select2();
  CKEDITOR.replace( 'UpdateProfileForm[about]' );
  print_state("updateprofileform-state");
  
  $("#updateprofileform-state").on('change',function() {
    print_city('updateprofileform-tehsil', this.selectedIndex);
  })
js;
$this->registerJs($js);
?>