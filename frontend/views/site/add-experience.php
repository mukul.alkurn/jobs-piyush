<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/14/2019
 * Time: 12:32 PM
 */

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

$months = '12';
$out_year = $out_month = [];
for ($i = 1; $i <= $months; $i++) {
    $out_month[$i] = $i;
}

$months = '60';
for ($i = 0; $i <= $months; $i++) {
    $out_year[$i] = $i;
}

?>
<style type="text/css">
            .form-group > .control-label { font-weight: bold !important }
            .e{color:black ;font-weight: 100}
</style>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
        <?= $this->render('_sidebar_candidate'); ?>

        <div class="dashboard-content">
            <?php
            /*  @var \backend\modules\user\models\User */
            $user = Yii::$app->user->identity;
            if (isset($user->userProfile->fresher)) {
                $checked = true;

            } else {
                $checked = false;
            }
            ?>
            <div class="col-sm-12">
                <?= Html::checkbox('fresher', $checked, ['class' => 'fresher']) ?> <strong>Are you a Fresher?</strong>

            </div>
            <br><br/>
            <div class="row" id="fresher1">
                <div class="col-lg-12 col-sm-12">
                    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 4, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsEducation[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'company_name',
                            'year',
                            'months',
                            'designation',
                            'department',
                            'place',

                        ],
                    ]); ?>

                    <div class="panel panel-default">
                        <button type="button" class="pull-right add-item btn btn-success btn-xs"><i
                                    class="fa fa-plus"></i>
                            Add Experience
                        </button>
                        <div class="panel-body container-items">

                            <!-- widgetContainer -->
                            <?php foreach ($modelsEducation as $index => $modelBath): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">
                                        Experience  Details
                                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                                                    class="fa fa-minus"></i></button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]company_name")->textInput(['maxlength' => true])->label("Company Name <span class='e'> (e.x Google)") ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]department")->textInput(['maxlength' => true])->label("Department Name <span class='e'> (e.x IT)") ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]year")->dropDownList($out_year)->label("Year Of Experience <span class='e'> (e.x 1)") ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]months")->dropDownList($out_month)->label("Month Of Experience <span class='e'> (e.x 9)") ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]designation")->textInput(['maxlength' => true])->label("Designation <span class='e'> (e.x Web Developer)") ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelBath, "[{$index}]place")->textInput(['maxlength' => true])->label("Place <span class='e'> (e.x Mumbai)") ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                    <div class="form-group">
                        <?= \yii\helpers\Html::submitButton('Add ', ['class' => 'btn btn-primary']) ?>
                    </div>
                  </div>

                    <?php ActiveForm::end(); ?>
                </div>
                       <div class="form-group col-sm-12 text-center">
                            <?= Html::a('Next',['add-certificate'], ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
                 
            </div>
        </div>

    </div>
</section>

<?php
$url = Url::to(['mark-fresher'], true);
$js = <<<js
var fresher = $(".fresher");
if((fresher).is(':checked')){
      $("#fresher1").hide();
}
fresher.on('change',function() {
    if($(this).is(":checked")){
          var fresher_value = 1
          $("#fresher1").hide();
    }else{
        var  fresher_value = null
        $("#fresher1").show();
    }
    $.post('$url',{fresher:fresher_value})  
})

js;
$this->registerJs($js);
?>

