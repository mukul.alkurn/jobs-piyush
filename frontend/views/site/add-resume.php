<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Add Resume';
$this->params['breadcrumbs'][] = $this->title;
/*  @var $user \backend\modules\user\models\User*/
$user = Yii::$app->user->identity;
$files = [];
if (isset($user->userProfile->resume)) {
        $type = '/uploads/'. $user->userProfile->resume;
        $files[] = $type;
        $initialPreviewConfig[] = [
            'width' => '120px',
        ];
}
?>
<style type="text/css">
            .form-group > .control-label { font-weight: bold !important }

</style>
<section class="dash-content">
<div class="container">
<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
<?= $this->render('_sidebar_candidate') ?>

<div class="dashboard-content">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <?php $form = ActiveForm::begin(['id' => 'user']); ?>
            <div class="col-sm-12">
                <?= $form->field($model, 'resume')->widget(\kartik\file\FileInput::classname(), [
                    'options' => ['accept' => 'image/*','multiple'=>false],
                    'pluginOptions' => [
                        'initialPreviewAsData' => true,
                        'initialPreview' => $files,
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                        'showCancel' => false,
                        'uploadAsync' => false,
                        'browseClass' => 'btn btn-primary radius px-5 py-1 text-uppercase',
                        'maxFileSize' => 1500,
                        'showClose' => false,
                        'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg"],
                        'overwriteInitial' => true,
                        'fileActionSettings' => [
                            'showDrag' => false,
                            'showRemove' => false,
                            'showZoom' => false
                        ],
                        'previewSettings' => [
                            'image' => [
                                'width' => "auto",
                                'height' => "100%",
                                'max-width' => "100%",
                                'max-height' => "100%"
                            ]
                        ],
                        'browseLabel' => 'Add Resume ',
                        'browseIcon' => '<i class="fa fa-camera"></i>&nbsp;',
                    ]
                ])->label(false); ?>
            </div>

            <div class="form-group col-sm-12">
                <?= Html::submitButton('Add Resume', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <div class="form-group col-sm-12 text-center">
                            <?= Html::a('Next',['add-language'], ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

</div>

</section>
