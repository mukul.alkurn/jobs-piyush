<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
        <?= $this->render('_sidebar_candidate') ?>
        <div class="dashboard-content">

            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="loginbox">
                            <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'old_password')->passwordInput() ?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'new_password')->passwordInput() ?>

                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
                            </div>

                            <div class="form-group col-sm-12">
                                <?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12"></div>
                </div>
            </div>

        </div>
</section>

