<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="login-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-12"></div>
            <div class="col-lg-6 col-md-8 col-sm-12">
                <div class="loginbox">
                  <h1><?= Html::encode($this->title) ?></h1>
          
                    <p class="sub-title">Please fill out the following fields to login:</p>
          
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                        <div style="color:#999;margin:1em 0">
                            If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton('Login', ['class' => 'btn-blue btn btn-primary', 'name' => 'login-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-12"></div>
        </div>
    </div>
</section>

