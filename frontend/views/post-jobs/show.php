<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 12/11/2019
 * Time: 10:20 PM
 */

use yii\helpers\ArrayHelper;
\frontend\assets\SweetAlertAsset::register($this);
?>
<div id="titlebar">
    <div class="container">
        <div class="ten columns">
            <h1>Industry Type</h1>
            <h3><?= $model->industry0->name ?></h3>
        </div>

    </div>
</div>


<!-- Content
================================================== -->
<div class="container">

    <!-- Recent Jobs -->
    <div class="eleven columns">
        <div class="padding-right">
            <div class="company-info">
                <div class="content">
                    <h4>Job Description</h4>
                </div>
                <div class="clearfix"></div>
            </div>

            <p class="margin-reset">
                <?= $model->title ?>
            </p>

            <br>
            <p><?= $model->description ?></p>

            <br>

            <h4 class="margin-bottom-10">Job Requirment</h4>

            <ul class="list-1">
                <li>Experience: <?= $model->experience0->name ?></li>
                <li>Industry <?= $model->industry0->name ?></li>
                <li>Category <?= $model->category0->name ?></li>
                <li>Job
                    Specialities <?php echo implode(',', ArrayHelper::getColumn($model->postJobSpecailities, function ($model) {
                        return $model->speciality->name;
                    })); ?></li>
                <li>Salary Offered. <i class="fa fa-rupee"></i> <?= $model->offered_salary ?></li>
            </ul>

        </div>
    </div>


    <!-- Widgets -->
    <div class="five columns">

        <!-- Sort by -->
        <div class="widget">
            <h4>Overview</h4>

            <div class="job-overview">

                <ul>
                    <li>
                        <i class="fa fa-map-marker"></i>
                        <div>
                            <strong>Location:</strong>
                            <span><?= $model->postJobAddress->address ?></span>
                        </div>
                    </li>
                    <li>
                        <i class="fa fa-user"></i>
                        <div>
                            <strong>City:</strong>
                            <span><?= $model->postJobAddress->city ?></span>
                        </div>
                    </li>
                    <li>
                        <i class="fa fa-clock-o"></i>
                        <div>
                            <strong>State:</strong>
                            <span><?= $model->postJobAddress->state ?></span>
                        </div>
                    </li>

                </ul>


                <a href="#small-dialog" class="popup-with-zoom-anim button">Apply For This Job</a>

                <div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
                    <div class="small-dialog-headline">
                        <h2>Apply For This Job</h2>
                    </div>

                    <div class="small-dialog-content">
                        <form action="#" method="post" id="form-submit">
                            <?php
                            /* @var $user \backend\modules\user\models\User; */
                            $user = Yii::$app->user->identity;
                            ?>
                            <input type="text" placeholder="Full Name" value="<?= $user->userProfile->getName() ?>"
                                   readonly/>
                            <input type="text" placeholder="Email Address" value="<?= $user->email ?>" readonly/>
                            <textarea placeholder="Your message  to the employer" id="message"></textarea>
                            <div class="clearfix"></div>
                            <div class="divider"></div>

                            <input class="send" id="submit" type="submit" name="Send Application"/>
                        </form>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- Widgets / End -->


</div>
<?php
$url = \yii\helpers\Url::to(['apply', 'id' => $model->id], true);
$user_id = Yii::$app->user->id;
$js = <<<js

var submit = $("#submit");
submit.on('click',function() {
    $.ajax({
            url:'$url',
            type:'post',
            data:{message:$("#message").val(),user_id:'$user_id'},
            cache:true,
            success:function(response) {
                    swal(response.message).then(function() {
                        window.location.reload();
                    })
            }
    })
  return false; 
})
js;
$this->registerJs($js);
?>
