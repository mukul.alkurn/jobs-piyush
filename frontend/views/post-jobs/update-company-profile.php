<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\PostJobs */

use yii\bootstrap\ActiveForm;
\frontend\assets\EditorAsset::register($this);
$this->title = 'Update Company Info';
$business_type = [
    'private-limited' => 'Private Limited',
    'limited-company' => 'Limited Company',
    'corporations' => 'Corporations',
    'partnerships' => 'Partnerships',
    'sole-proprietorships' => 'Sole Proprietorships',
    'artitecture' => 'Artitecture',
    'marketing' => 'Marketing',
];

?>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

        <div class="dashboard-nav">
            <?= $this->render('_sidebar') ?>
        </div>

        <div class="dashboard-content">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box-content">
                        <div class="col-lg-12 col-sm-12">

                            <div class="col-sm-12">
                                <p><strong>Company Info</strong></p>
                            </div>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'mobile')->textInput() ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'emailAddress')->textInput(['readonly' => true]) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'gender')->dropDownList(['male' => 'male', 'female' => 'female']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'company_name')->textInput() ?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'about')->textarea() ?>
                            </div>

                            <div class="col-sm-12">
                                <p><strong>Contact Information</strong></p>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'address')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'city')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'state')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'postalCode')->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'country')->textInput() ?>
                            </div>
                            <div class="col-sm-12">
                                <p><strong>Extra Info</strong></p>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'estd_since')->textInput(['type' => 'year']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'team_size')->textInput(['type' => 'number']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'business_type')->dropDownList($business_type) ?>
                            </div>

                            <div class="form-group col-sm-12">
                                <?= Html::submitButton('Update Company Info', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$js = <<<js
  CKEDITOR.replace('UpdateProfileForm[about]');
js;
$this->registerJs($js);

?>