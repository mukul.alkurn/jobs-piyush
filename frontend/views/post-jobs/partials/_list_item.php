<?php
// _list_item.php
use yii\helpers\Html;
use yii\helpers\Url;

?>


<a href="<?=  Url::to(['/post-jobs/show', 'id' => $model->id])?>" class="listing freelance">
    <div class="listing-title">
        <h4><?= ucwords($model->title) ?> </h4>
        <ul class="listing-icons">

            <li><i class="fa fa-industry"></i> <?= $model->industry0->name ?></li>
            <li><i class="ln ln-icon-Map2"></i> <?= $model->postJobAddress->city ?></li>
            <li><i class="ln ln-icon-Money-2"></i> <?= $model->offered_salary ?></li>
            <li><div class="listing-date"><?=  date('F d,Y',$model->created_at)?></div></li>
        </ul>
    </div>
</a>