<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use backend\modules\user\models\Industry;

$this->title = 'Job Listing';
\frontend\assets\Select2Asset::register($this);
/* @var $this yii\web\View */
/* @var $model frontend\models\search\PostJobsSearch */
/* @var $form yii\widgets\ActiveForm */
$industry = ArrayHelper::map(Industry::find()->all(), 'id', 'name');
$category = ArrayHelper::map(\backend\modules\user\models\JobCategories::find()->all(), 'id', 'name');
$this->registerJsFile("@web/js/city.js");
?>
<div id="titlebar">
    <div class="container">
        <div class="ten columns">
            <span>We found <?= $dataProvider->getCount() ?> jobs posted</span>
        </div>

    </div>
</div>

<div class="container">
    <div class="eleven columns">
        <div class="padding-right">
            <h3 class="margin-bottom-25">Recent Jobs</h3>
            <div class="listings-container">
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper',
                        'id' => 'list-wrapper',
                    ],
                    'layout' => "{pager}\n{items}\n{summary}",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('partials/_list_item', ['model' => $model]);
                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'summary' => false,
                    'pager' => [
                        'firstPageLabel' => 'first',
                        'lastPageLabel' => 'last',
                        'nextPageLabel' => 'next',
                        'prevPageLabel' => 'previous',
                        'maxButtonCount' => 3,
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="five columns side-form">

        <div class="widget">
            <h4>Filter Jobs</h4>

            <?php $form = ActiveForm::begin([
                'action' => ['search'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'state')->dropDownList([]) ?>
            <?= $form->field($searchModel, 'city')->dropDownList([]) ?>
            <?= $form->field($searchModel, 'pincode') ?>
            <?= $form->field($searchModel, 'industry')->dropDownList($industry,['id'=>'industry','multiple'=>true])->label('Industry') ?>
            <?= $form->field($searchModel, 'category')->dropDownList($category,['id'=>'category','multiple'=>true])->label('Job category') ?>


            <div class="sidebar-btn">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>


    </div>
</div>
<?php
$js = <<<js

 $('#category,#industry').select2();
 print_state("postjobssearch-state")
  
  $("#postjobssearch-state").on('change',function() {
    print_city('postjobssearch-city', this.selectedIndex);
  })

js;
$this->registerJs($js);
?>

