<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\PostJobs */

use yii\bootstrap\ActiveForm;

$this->title = 'HR Info';

use wbraganca\dynamicform\DynamicFormWidget;

?>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

        <div class="dashboard-nav">
            <?= $this->render('_sidebar') ?>
        </div>

        <div class="dashboard-content">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box-content">
                        <div class="col-lg-12 col-sm-12">

                            <div class="col-sm-12">
                                <p><strong>HR Info</strong></p>
                            </div>
                            <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
                            <div class="col-sm-12">
                                <?php DynamicFormWidget::begin([
                                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                    'widgetBody' => '.container-items', // required: css class selector
                                    'widgetItem' => '.item', // required: css class
                                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                                    'min' => 1, // 0 or 1 (default 1)
                                    'insertButton' => '.add-item', // css class
                                    'deleteButton' => '.remove-item', // css class
                                    'model' => $model[0],
                                    'formId' => 'dynamic-form',
                                    'formFields' => [
                                        'full_name',
                                        'gender',
                                        'year_of_joining',
                                        'designation',
                                        'email',
                                        'phone',
                                    ],
                                ]); ?>

                                <div class="panel panel-default">
                                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i
                                                class="fa fa-plus"></i>
                                        Hr Details
                                    </button>
                                    <div class="panel-body container-items"><!-- widgetContainer -->
                                        <?php foreach ($model as $index => $name): ?>
                                            <div class="item panel panel-default"><!-- widgetBody -->
                                                <div class="panel-heading">
                                                    Hr Info
                                                    <button type="button"
                                                            class="pull-right remove-item btn btn-danger btn-xs">
                                                        <i
                                                                class="fa fa-minus"></i></button>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]full_name")->textInput(['maxlength' => true])->label() ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]gender")->dropDownList(['male'=>'Male','female'=>'Female'])->label() ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]year_of_joining")->textInput(['type'=>'date'])->label() ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]designation")->textInput() ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]email")->textInput() ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?= $form->field($name, "[{$index}]phone")->textInput() ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php DynamicFormWidget::end(); ?>
                                <div class="form-group col-sm-12">
                                    <?= Html::submitButton('Add Info', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
