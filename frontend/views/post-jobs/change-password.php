<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\PostJobs */

use yii\bootstrap\ActiveForm;

$this->title = 'Change Password';

?>
<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

        <div class="dashboard-nav">
            <?= $this->render('_sidebar') ?>
        </div>

        <div class="dashboard-content">
            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <div class="col-md-12">
                        <p><strong><?= $this->title ?></strong></p>
                    </div>
                    <div class="dashboard-list-box-content">
                        <div class="col-lg-12 col-sm-12">

                            <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'old_password')->passwordInput() ?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'new_password')->passwordInput() ?>

                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
                            </div>

                            <div class="form-group col-sm-12">
                                <?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
