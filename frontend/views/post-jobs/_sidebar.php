<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 12/12/2019
 * Time: 8:38 AM
 */

use yii\helpers\Url;

?>
    <div class="dashboard-nav-inner">


        <ul data-submenu-title="Start">
            <li><a href="<?= Url::to(['#']) ?>">Dashboard</a></li>
        </ul>

        <ul data-submenu-title="Management">
            <li><a href="<?= Url::to(['index'], true) ?>">List Job</a></li>
            <li><a href="<?= Url::to(['hr-info'], true) ?>">HR Info</a></li>
            <li><a href="<?= Url::to(['/post-jobs/createx'], true) ?>">Post Job</a></li>
            <li><a href="<?= Url::to(['update-company-profile'], true) ?>">Update Company Info</a></li>
            <li><a href="<?= Url::to(['change-password'], true) ?>">Change Password</a></li>
        </ul>

        <ul data-submenu-title="Account">
            <li><a href="<?= Url::to(['/site/logout']) ?>" data-method="post">Logout</a></li>
        </ul>

    </div>
