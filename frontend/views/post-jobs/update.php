<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\PostJobs */

$this->title = 'Update Post Jobs: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Post Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>


<div class="dashboard-nav">
    <?= $this->render('_sidebar') ?>
</div>
<div class="dashboard-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="dashboard-list-box-content">

                    <div class="post-jobs-update">

                        <h1><?= Html::encode($this->title) ?></h1>

                        <?= $this->render('_form', [
                            'model' => $model,
                            'modelAddress' => $modelAddress
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
