<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\user\models\Industry;
use backend\modules\user\models\JobSpeciality;
use backend\modules\user\models\Experience;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\PostJobs */
/* @var $form yii\widgets\ActiveForm */
/* @var $user \backend\modules\user\models\User */
\frontend\assets\EditorAsset::register($this);
\frontend\assets\Select2Asset::register($this);
$user = Yii::$app->user->identity;
$industry = ArrayHelper::map(Industry::find()->all(), 'id', 'name');
$jobtype = ArrayHelper::map($user->userJobCategories, 'id', 'category.name');
$experience = ArrayHelper::map(Experience::find()->all(), 'id', 'name');
$jobSpeciality = ArrayHelper::map(JobSpeciality::find()->all(), 'id', 'name');
$hr = ArrayHelper::map(\backend\modules\user\models\HrInfo::find()->all(), 'id', 'full_name');
$jobCategory = ArrayHelper::map(\backend\modules\user\models\JobCategories::find()->all(), 'id', 'name');
$salary = [
    '5k---10k' => '5k---10k',
    '15k---16k' => '15k---16k',
    '16k---20k' => '16k---20k',
    '20k---25k' => '20k---25k',
    '25k---30k' => '25k---30k',
    '30k---45k' => '30k---45k',
    '45k---50k' => '45k---50k',
    '50k---60k' => '50k---60k',
    'more-than-60...' => 'more-than-60...'

];
$this->registerJsFile("@web/js/city.js");
?>
<section class="dash-content">
    <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

    <div class="post-jobs-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="col-sm-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'hr_id')->dropDownList($hr)->label('Posted By') ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'deadline')->textInput(['type' => 'date']) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'category')->dropDownList($jobCategory, ['prompt' => 'Choose Category']) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'experience')->dropDownList($experience, ['prompt' => 'Choose Experience']) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'industry')->dropDownList($industry, ['prompt' => 'Choose Industry']) ?>

        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'specialities')->dropDownList($jobSpeciality, ['multiple' => true, 'id' => 'speciality']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'offered_salary')->dropDownList($salary) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'address')->textInput() ?></div>
        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'state')->dropDownList([]) ?></div>
        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'city')->dropDownList([]) ?></div>

        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'tehsil')->textInput() ?></div>

        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'district')->textInput() ?></div>

        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'pincode')->textInput() ?></div>
        <div class="col-sm-6">
            <?= $form->field($modelAddress, 'country')->textInput(['value' => 'India', 'readonly' => true]) ?></div>

        <div class="form-group col-sm-12">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</section>

<?php
$js = <<<js
  CKEDITOR.replace('PostJobs[description]');
  $('#speciality').select2();
   print_state("postjobaddress-state")
 $("#postjobaddress-state").on('change',function() {
    print_city('postjobaddress-city', this.selectedIndex);
  })
js;
$this->registerJs($js);

?>
