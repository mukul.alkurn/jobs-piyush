<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\PostJobs */

$this->title = 'Add Post Jobs';
$this->params['breadcrumbs'][] = ['label' => 'Post Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
<div class="dashboard-nav">
    <?= $this->render('_sidebar') ?>
</div>

<div class="dashboard-content">
    <div class="row">
        <div class="col-lg-12 col-md-12">

            <div class="col-sm-12">
                <p><strong><?= $this->title ?></strong></p>
            </div>
            <div class="dashboard-list-box-content">
                <div class="post-jobs-update">

                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelAddress' => $modelAddress
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
