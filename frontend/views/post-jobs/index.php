<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PostJobsSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Post Jobs';
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="dash-content">
    <div class="container">
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
        <div class="dashboard-nav">
            <?= $this->render('_sidebar') ?>
        </div>
        <div class="dashboard-content">
            <div id="titlebar">
                    <div class="">
                        <h2>Manage Jobs</h2>
                        <a href="<?= Url::to(['create'], true) ?>" class="button margin-top-30">Add New Listing</a>
                    </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box-content">
                        <table class="manage-table responsive-table stacktable">
                            <tbody>
                            <tr>
                                <th><i class="fa fa-file-text"></i> Title</th>
                                <th><i class="fa fa-calendar"></i> Date Posted</th>
                                <th><i class="fa fa-calendar"></i> Job Expire</th>
                                <th><i class="fa fa-user"></i> Applications</th>
                                <th></th>
                            </tr>
                            <?php
                            foreach ($dataProvider->getModels() as $key => $value) {
                                ?>
                                <tr>
                                    <td class="title"><a
                                                href="<?= Url::to(['view', 'id' => $value->id], true) ?>"><?= $value->title ?></a>
                                    </td>
                                    <td><?= date('F d, Y', $value->created_at) ?></td>
                                    <td><?= date('F d, Y', strtotime($value->deadline)) ?></td>
                                    <td class="centered"><a href="#" class="button">Show (4)</a></td>
                                    <td class="action">

                                        <a href="<?= Url::to(['update', 'id' => $value->id], true) ?>"><i
                                                    class="fa fa-pencil"></i>
                                            Edit</a>
                                        <a href="<?= Url::to(['delete', 'id' => $value->id], true) ?>" class="delete"><i
                                                    class="fa fa-remove"></i> Delete</a>
                                    </td>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
