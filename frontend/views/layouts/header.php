<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/10/2019
 * Time: 12:04 PM
 */
?>
<header class="transparent sticky-header">
    <div class="container">
        <div class="sixteen columns">

            <!-- Logo -->
            <div id="logo">
                <h1><a href="index.html"><img src="images/logo2.png" alt="Work Scout"/></a></h1>
            </div>

            <!-- Menu -->
            <nav id="navigation" class="menu">
                <ul id="responsive">

                    <li><a id="current" href="index.html">Home</a>
                        <ul>
                            <li><a href="index.html">Home #1</a></li>
                            <li><a href="index-2.html">Home #2</a></li>
                            <li><a href="index-3.html">Home #3</a></li>
                            <li><a href="index-4.html">Home #4</a></li>
                            <li><a href="index-5.html">Home #5</a></li>
                        </ul>
                    </li>

                    <li><a href="#">Pages</a>
                        <ul>
                            <li><a href="job-page.html">Job Page</a></li>
                            <li><a href="job-page-alt.html">Job Page Alternative</a></li>
                            <li><a href="resume-page.html">Resume Page</a></li>
                            <li><a href="shortcodes.html">Shortcodes</a></li>
                            <li><a href="icons.html">Icons</a></li>
                            <li><a href="pricing-tables.html">Pricing Tables</a></li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </li>

                    <li><a href="#">Browse Listings</a>
                        <ul>
                            <li><a href="browse-jobs.html">Browse Jobs</a></li>
                            <li><a href="browse-resumes.html">Browse Resumes</a></li>
                            <li><a href="browse-categories.html">Browse Categories</a></li>
                        </ul>
                    </li>

                    <li><a href="#">Dashboard</a>
                        <ul>
                            <li><a href="dashboard.html">Dashboard</a></li>
                            <li><a href="dashboard-messages.html">Messages</a></li>
                            <li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
                            <li><a href="dashboard-add-resume.html">Add Resume</a></li>
                            <li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
                            <li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
                            <li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
                            <li><a href="dashboard-add-job.html">Add Job</a></li>
                            <li><a href="dashboard-my-profile.html">My Profile</a></li>
                        </ul>
                    </li>
                </ul>


                <ul class="float-right">
                    <li><a href="my-account.html#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
                    <li><a href="my-account.html"><i class="fa fa-lock"></i> Log In</a></li>
                </ul>

            </nav>

            <!-- Navigation -->
            <div id="mobile-navigation">
                <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
            </div>

        </div>
    </div>
</header>
<div class="clearfix"></div>
<div id="banner" class="with-transparent-header parallax background"
     style="background-image: url(images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330"
     data-diff="300">
    <div class="container">
        <div class="sixteen columns">

            <div class="search-container">

                <!-- Form -->
                <h2>Find Job</h2>
                <input type="text" class="ico-01" placeholder="job title, keywords or company name" value=""/>
                <input type="text" class="ico-02" placeholder="city, province or region" value=""/>
                <button><i class="fa fa-search"></i></button>

                <!-- Browse Jobs -->
                <div class="browse-jobs">
                    Browse job offers by <a href="browse-categories.html"> category</a> or <a href="#">location</a>
                </div>

                <!-- Announce -->
                <!-- 				<div class="announce">
                                    We’ve over <strong>15 000</strong> job offers for you!
                                </div> -->

            </div>

        </div>
    </div>
</div>
