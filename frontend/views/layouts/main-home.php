<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= isset($this->title) ? Html::encode($this->title) . ' | ' . 'Jobs Captial' : 'Jobs Captial' ?></title>
    <?php $this->head() ?>
</head>

<div id="wrapper">
    <body>
    <?php $this->beginBody() ?>

    <header class="nav">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <p><a href="<?= Url::to(['/site/index'], true) ?>"><img src="../images/jobs-capital-logo.png" alt=""></a>
                    </p>
                </div>

                <!-- Menu -->
                <nav id="navigation" class="menu">
                    <ul id="responsive">
                        <li><a href="<?= Url::to(['/site/index'], true) ?>">Home</a>
                        <li><a href="<?= Url::to(['/post-jobs/search'], true) ?>">Browse Jobs</a></li>

                        <!--				<li><a href="#">Pages</a>-->
                        <!--					<ul>-->
                        <!--						<li><a href="job-page.html">Job Page</a></li>-->
                        <!--						<li><a href="job-page-alt.html">Job Page Alternative</a></li>-->
                        <!--						<li><a href="resume-page.html">Resume Page</a></li>-->
                        <!--						<li><a href="shortcodes.html">Shortcodes</a></li>-->
                        <!--						<li><a href="icons.html">Icons</a></li>-->
                        <!--						<li><a href="pricing-tables.html">Pricing Tables</a></li>-->
                        <!--						<li><a href="blog.html">Blog</a></li>-->
                        <!--						<li><a href="contact.html">Contact</a></li>-->
                        <!--					</ul>-->
                        <!--				</li>-->

                        <!--				<li><a href="#">Browse Listings</a>-->
                        <!--					<ul>-->
                        <!--						-->
                        <!--						<li><a href="-->
                        <? //= Url::to(['/post-jobs/search'],true)?><!--">Browse Resumes</a></li>-->
                        <!--						<li><a href="browse-categories.html">Browse Categories</a></li>-->
                        <!--					</ul>-->
                        <!--				</li>-->

                        <!--                        <li><a href="#">Dashboard</a>-->
                        <!--                            <ul>-->
                        <!--                                <li><a href="dashboard.html">Dashboard</a></li>-->
                        <!--                                <li><a href="dashboard-messages.html">Messages</a></li>-->
                        <!--                                <li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>-->
                        <!--                                <li><a href="dashboard-add-resume.html">Add Resume</a></li>-->
                        <!--                                <li><a href="dashboard-job-alerts.html">Job Alerts</a></li>-->
                        <!--                                <li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>-->
                        <!--                                <li><a href="dashboard-manage-applications.html">Manage Applications</a></li>-->
                        <!--                                <li><a href="dashboard-add-job.html">Add Job</a></li>-->
                        <!--                                <li><a href="dashboard-my-profile.html">My Profile</a></li>-->
                        <!--                            </ul>-->
                        <!--                        </li>-->
                        <?php
                        if (Yii::$app->user->can('employer')) { ?>
                        <li><a href="<?= Url::to(['/post-jobs/index'], true) ?>">Post Job</a>

                            <?php } ?>
                        </li>
                        <?php
                        if (!Yii::$app->user->getIsGuest()) {
                            if (Yii::$app->user->can('candidate')) {
                                $candidate = Url::to(['/site/update-profile-candidate'], true);
                            } elseif (Yii::$app->user->can('employer')) {
                                $candidate = Url::to(['/post-jobs/index'], true);
                            }else{
                                $candidate = Url::to(['/site/index'],true);
                            }
                            ?>
                            <li><a href="<?= $candidate ?>">Dashboard</a></li>

                        <?php } ?>

                    </ul>

                    <ul class="float-right">
                        <?php if (Yii::$app->user->getIsGuest()) { ?>
                            <li><a href="<?= Url::to(['/site/employer-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Employer Sign
                                    Up</a></li>
                            <li><a href="<?= Url::to(['/site/candidate-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Candidate
                                    Sign Up</a></li>
                            <li><a href="<?= Url::to(['/site/login'], true) ?>"><i class="fa fa-lock"></i> Log In</a>
                            </li>
                        <?php } else { ?>
                             <li><a><?= 'Welcome,'. Yii::$app->user->identity->userProfile->getName()?></a></li>
                            
                            <li><a data-method="post" href="<?= Url::to(['/site/logout'], true) ?>"><i
                                            class="fa fa-lock"></i> Log Out</a>
                            </li>

                        <?php } ?>


                    </ul>
                </nav>

                <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>


    <!-- Banner
    ================================================== -->
    <div id="banner" class="with-transparent-header parallax background"
         style="background-image: url(images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330"
         data-diff="300">
        <div class="container">
            <div class="sixteen columns">

                <div class="search-container">
                    <!-- Form -->
                    <h2>jobs capital india's largest job junction</h2>
                    <div class="banner-btns">
                        <button onclick="window.location.href ='/post-jobs/search/'">hire now</button>
                        <button onclick="window.location.href ='/post-jobs/search/'">get job now</button>    
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <?= \frontend\components\GrowlAlertWidget::widget() ?>
    <?= $content ?>
    <?= $this->render('modal')?>
    <?= $this->render('footer')?>  

    <?php $this->endBody() ?>
    </body>
</div>
</html>
<?php $this->endPage() ?>
