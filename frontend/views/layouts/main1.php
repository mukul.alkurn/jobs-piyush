<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>JobsCapital</title>
    <?php $this->head() ?>
</head>

<div id="wrapper">
    <body>
    <?php $this->beginBody() ?>

    <header class="nav">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1><a href="<?= Url::to(['index'], true) ?>"><img src="images/logo2.png" alt="Work Scout"/></a>
                    </h1>
                </div>

                <!-- Menu -->
                <nav id="navigation" class="menu">
                    <ul id="responsive">
                        <li><a href="<?= Url::to(['/site/index'], true) ?>">Home</a>
                        <li><a href="<?= Url::to(['/post-jobs/search'], true) ?>">Browse Jobs</a></li>

                        <!--				<li><a href="#">Pages</a>-->
                        <!--					<ul>-->
                        <!--						<li><a href="job-page.html">Job Page</a></li>-->
                        <!--						<li><a href="job-page-alt.html">Job Page Alternative</a></li>-->
                        <!--						<li><a href="resume-page.html">Resume Page</a></li>-->
                        <!--						<li><a href="shortcodes.html">Shortcodes</a></li>-->
                        <!--						<li><a href="icons.html">Icons</a></li>-->
                        <!--						<li><a href="pricing-tables.html">Pricing Tables</a></li>-->
                        <!--						<li><a href="blog.html">Blog</a></li>-->
                        <!--						<li><a href="contact.html">Contact</a></li>-->
                        <!--					</ul>-->
                        <!--				</li>-->

                        <!--				<li><a href="#">Browse Listings</a>-->
                        <!--					<ul>-->
                        <!--						-->
                        <!--						<li><a href="-->
                        <? //= Url::to(['/post-jobs/search'],true)?><!--">Browse Resumes</a></li>-->
                        <!--						<li><a href="browse-categories.html">Browse Categories</a></li>-->
                        <!--					</ul>-->
                        <!--				</li>-->

                        <!--                        <li><a href="#">Dashboard</a>-->
                        <!--                            <ul>-->
                        <!--                                <li><a href="dashboard.html">Dashboard</a></li>-->
                        <!--                                <li><a href="dashboard-messages.html">Messages</a></li>-->
                        <!--                                <li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>-->
                        <!--                                <li><a href="dashboard-add-resume.html">Add Resume</a></li>-->
                        <!--                                <li><a href="dashboard-job-alerts.html">Job Alerts</a></li>-->
                        <!--                                <li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>-->
                        <!--                                <li><a href="dashboard-manage-applications.html">Manage Applications</a></li>-->
                        <!--                                <li><a href="dashboard-add-job.html">Add Job</a></li>-->
                        <!--                                <li><a href="dashboard-my-profile.html">My Profile</a></li>-->
                        <!--                            </ul>-->
                        <!--                        </li>-->
                    </ul>
                    <ul class="float-right">
                        <?php if (Yii::$app->user->getIsGuest()) { ?>
                            <li><a href="<?= Url::to(['/site/employer-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Employer Sign
                                    Up</a></li>
                            <li><a href="<?= Url::to(['/site/candidate-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Candidate
                                    Sign Up</a></li>
                            <li><a href="<?= Url::to(['/site/login'], true) ?>"><i class="fa fa-lock"></i> Log In</a>
                            </li>
                        <?php } else { ?>
                            <li><a data-method='Post' href="<?= Url::to(['/site/logout'], true) ?>"><i
                                            class="fa fa-lock"></i> Log Out</a>
                            </li>

                        <?php } ?>
                    </ul>

                    <?php
                    if (Yii::$app->user->can('employer')) { ?>
                    <li><a href="<?= Url::to(['/post/index'], true) ?>">Post Job</a>

                        <?php } ?>
                </nav>

                <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>


    <?= \frontend\components\GrowlAlertWidget::widget() ?>
        <?= $content ?>
    <?= $this->render('modal') ?>
    <?= $this->render('footer') ?>

    <?php $this->endBody() ?>
    </body>
</div>
</html>
<?php $this->endPage() ?>
