<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= isset($this->title) ? Html::encode($this->title) . ' | ' . 'JobsCapital' : 'JobsCapital' ?></title>
    <link href="">
    <?php $this->head() ?>
</head>

<div id="wrapper">
    <body>
    <?php $this->beginBody() ?>

    <header class="nav site-header">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1><a href="<?= Url::to(['/site/index'], true) ?>"><img src="../images/jobs-capital-logo.png"
                                                                             alt=""></a>
                    </h1>
                </div>

                <!-- Menu -->
                <nav id="navigation" class="menu">
                    <ul id="responsive">
                        <li><a href="<?= Url::to(['/site/index'], true) ?>">Home</a>
                        <li><a href="<?= Url::to(['/post-jobs/search'], true) ?>">Browse Jobs</a></li>
                        <?php
                        if (!Yii::$app->user->getIsGuest()) {
                            if (Yii::$app->user->can('candidate')) {
                                $candidate = Url::to(['/site/update-profile-candidate'], true);
                            } elseif (Yii::$app->user->can('employer')) {
                                $candidate = Url::to(['/post-jobs/index'], true);
                            }else{
                                $candidate = Url::to(['/site/index'],true);
                            }
                            ?>
                            <li><a href="<?= $candidate ?>">Dashboard</a></li>

                        <?php } ?>

                    </ul>
                    <ul class="float-right">
                        <?php if (Yii::$app->user->getIsGuest()) { ?>
                            <li><a href="<?= Url::to(['/site/employer-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Employer Sign
                                    Up</a></li>
                            <li><a href="<?= Url::to(['/site/candidate-signup'], true) ?>"><i class="fa fa-user"></i>
                                    Candidate
                                    Sign Up</a></li>
                            <li><a href="<?= Url::to(['/site/login'], true) ?>"><i class="fa fa-lock"></i> Log In</a>
                            </li>
                        <?php } else { ?>
                             <li><a><?= 'Welcome,'. Yii::$app->user->identity->userProfile->getName()?></a></li>
                            <li><a data-method="post" href="<?= Url::to(['/site/logout'], true) ?>"><i
                                            class="fa fa-lock"></i> Log Out</a>
                            </li>

                        <?php } ?>
                    </ul>
                </nav>

                <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>


    <?= \frontend\components\GrowlAlertWidget::widget() ?>
    <?= $content ?>
    <?= $this->render('modal') ?>
    <?= $this->render('footer') ?>

    <?php $this->endBody() ?>
    </body>
</div>
</html>
<?php $this->endPage() ?>
