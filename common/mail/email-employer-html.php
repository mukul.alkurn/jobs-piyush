<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="password-reset">
    <p>Hello , You have successfully registered your account</p>

    <p>You can login with below credentials</p>

    <p>Email :- <?= $email?></p>
    <p>Password :- <?= $password?></p>
</div>
