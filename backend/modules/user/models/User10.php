<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_10".
 *
 * @property string $user_id
 * @property string $board_university
 * @property string $year_of_passing
 * @property string $percentage
 *
 * @property User $user
 */
class User10 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $ten;
    public $tweleve;
    public $iti;
    public $grd;
    public $pg;
    public $dip;
    public $name;
    public $board_10;
    public $year_10;
    public $per_10;
    public $iti_name;
    public $iti_per;
    public $iti_year;
    public $iti_board;
    public $dip_name;
    public $dip_per;
    public $dip_year;
    public $dip_board;
    public $board_12;
    public $year_12;
    public $per_12;
    public $stream_12;
    public $board_grad;
    public $branch_grad;
    public $year_grad;
    public $per_grad;
    public $stream_grad;
    public $board_pggrad;
    public $branch_pggrad;
    public $year_pggrad;
    public $per_pggrad;
    public $stream_pggrad;


    public static function tableName()
    {
        return 'user_10';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['ten', 'board_10', 'year_10', 'per_10','dip'], 'string'],
            [['board_12', 'year_12', 'per_12', 'stream_12'], 'string'],
            [['board_grad', 'year_grad', 'per_grad', 'branch_grad'], 'string'],
            [['board_pggrad', 'year_pggrad', 'per_pggrad', 'branch_pggrad'], 'string'],
            [['board_10', 'year_10', 'per_10'], 'required', 'when' => function ($model) {
                return $model->ten == '10';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-ten').is(':checked') == true;
            }"],
            [['iti_name','iti_year','iti_per','iti_board'], 'required', 'when' => function ($model) {
                return $model->iti == 'iti';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-iti').is(':checked') == true;
            }"],
            [['dip_name','dip_name','dip_year','dip_board'], 'required', 'when' => function ($model) {
                return $model->dip == 'dip';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-dip').is(':checked') == true;
            }"],

            [['board_pggrad', 'stream_pggrad','year_pggrad', 'per_pggrad', 'branch_pggrad'], 'required', 'when' => function ($model) {
                return $model->grd == 'pg';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-pg').is(':checked') == true;
            }"],

            [['board_12', 'year_12', 'per_12', 'stream_12'], 'required', 'when' => function ($model) {
                return $model->tweleve == '12';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-tweleve').is(':checked') == true;
            }"],

            [['board_grad', 'year_grad', 'per_grad','stream_grad', 'branch_grad'], 'required', 'when' => function ($model) {
                return $model->pg == 'grd';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user10-grd').is(':checked') == true;
            }"],

            [['board_university', 'year_of_passing', 'percentage'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'board_university' => 'Board/University',
            'year_of_passing' => 'Year Of Passing',
            'percentage' => 'Percentage',
            'board_10' => 'Board/University',
            'year_10' => 'Year Of Passing',
            'per_10' => 'Percentage',
            'board_12' => 'Board/University',
            'year_12' => 'Year Of Passing',
            'per_12' => 'Percentage',
            'stream_12' => 'Stream',
            'iti_name' => 'Subject Name',
            'iti_year' => 'Year Of Passing',
            'iti_per' => 'Percentage',
            'iti_board' => 'Board/University',
            'dip_name' => 'Subject Name',
            'dip_year' => 'Year Of Passing',
            'dip_per' => 'Percentage',
            'dip_board' => 'Board/ University',
            'board_grad' => 'Board/ University',
            'year_grad' =>'Year Of Passing',
            'per_grad'=>'Percentage',
            'stream_grad'=>'Subject',
            'branch_grad' =>'Branch Name',
            'board_pggrad' => 'Board University',
            'year_pggrad' =>'Year Of Passing',
            'per_pggrad'=>'Percentage',
            'branch_pggrad' =>'Branch Name',
            'stream_pggrad' =>'Stream',


        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
