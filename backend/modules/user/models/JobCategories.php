<?php

namespace backend\modules\user\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
/**
 * This is the model class for table "job_categories".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * * @property PostJobs[] $postJobs
 * @property UserJobCategory[] $userJobCategories
 * @property int $created_at
 * @property int $updated_at
 */

class JobCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
      
        return [
          TimestampBehavior::className(),
          [
              'class' => SluggableBehavior::className(),
              'attribute' => 'name',
              'ensureUnique' => true,
              'immutable' => true
          ],
        ];
    }

    public function getPostJobs()
    {

           return $this->hasMany(PostJobs::className(), ['category' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserJobCategories()
    {
        return $this->hasMany(UserJobCategory::className(), ['category_id' => 'id']);
    }




}
