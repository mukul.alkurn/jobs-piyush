<?php

namespace backend\modules\user\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use alkurn\blog\models\BlogPost;
use common\models\User as BaseUser;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property UserJobCategory[] $userJobCategories
 * @property BlogPost[] $blogPosts
 * @property UserAddress $userAddress
 * @property UserDocumentVerification[] $userDocumentVerifications
 * @property UserNotification $userNotification
 * @property UserProfile $userProfile
 * @property PostJobs[] $postJobs
 * @property UserEducation[] $userEducations
 * @property UserExperience[] $userExperiences
 * @property UserCertification[] $userCertifications
 * @property UserLanguage[] $userLanguages
 * @property UserCertficationName[] $userCertficationNames
 * @property UserGrad $userGrad
 * @property UserIti $userIti
 * @property User10 $user10
 * @property User12 $user12
 * @property UserPgGrad $userPgGrad
 * @property UserDiploma $userDiploma
 * @property HrInfo[] $hrInfos
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key'], 'required'],
            [['password_hash'], 'required', 'when' => function ($model) {
                return $model->isNewRecord;
            }, 'whenClient' => 'function(attribute, value) {
                return $("#user-id").length == 0;
            }'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            ['status', 'required'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAddress()
    {
        return $this->hasOne(UserAddress::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDocumentVerifications()
    {
        return $this->hasMany(UserDocumentVerification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotification()
    {
        return $this->hasOne(UserNotification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfileApprovals()
    {
        return $this->hasMany(UserProfileApproval::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSocialAuths()
    {
        return $this->hasMany(UserSocialAuth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVerification()
    {
        return $this->hasOne(UserVerification::className(), ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return UserStatus::getLabel($this->status);
    }

    /**
     * Get assigned roles of user
     *
     * @return array
     */
    public function getAssignedRoles()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);
        unset($roles['guest']);
        return ArrayHelper::getColumn($roles, function ($element) {
            return ucwords($element->name);
        });
    }

    /**
     * Get current role of user
     *
     * @return mixed|\yii\rbac\Role
     */
    public function getUserRole()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);
        unset($roles['guest']);
        return current($roles);
    }

    /**
     * Check if user has the requested role
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return array_key_exists($role, Yii::$app->authManager->getRolesByUser($this->id));
    }

    public function getIsEmailVerified()
    {
        return (isset($this->userVerification->responded) && $this->userVerification->responded == 1) ? 'Yes' : 'No';
    }

    public function isEmailVerified()
    {
        return (isset($this->userVerification->responded) && $this->userVerification->responded == 1);
    }


    /**
     * @return \yii\db\ActiveQuery
     *
     * /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserJobCategories()
    {
        return $this->hasMany(UserJobCategory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostJobs()
    {
        return $this->hasMany(PostJobs::className(), ['posted_by' => 'id']);
    }

    public function getJobs()
    {
        $result = new ActiveDataProvider([
            'query' => $this->getPostJobs()
        ]);
        return $result;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEducations()
    {
        return $this->hasMany(UserEducation::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserExperiences()
    {
        return $this->hasMany(UserExperience::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCertifications()
    {
        return $this->hasMany(UserCertification::className(), ['user_id' => 'id']);
    }

    public function getEducation()
    {
        $result = new ActiveDataProvider([
            'query' => $this->getUserEducations()
        ]);
        return $result;
    }

    public function getExperience()
    {
        $result = new ActiveDataProvider([
            'query' => $this->getUserExperiences()
        ]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLanguages()
    {
        return $this->hasMany(UserLanguage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCertficationNames()
    {
        return $this->hasMany(UserCertficationName::className(), ['user_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser10()
    {
        return $this->hasOne(User10::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser12()
    {
        return $this->hasOne(User12::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGrad()
    {
        return $this->hasOne(UserGrad::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIti()
    {
        return $this->hasOne(UserIti::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPgGrad()
    {
        return $this->hasOne(UserPgGrad::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiploma()
    {
        return $this->hasOne(UserDiploma::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHrInfos()
    {
        return $this->hasMany(HrInfo::className(), ['user_id' => 'id']);
    }


}
