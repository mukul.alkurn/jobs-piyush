<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_12".
 *
 * @property string $user_id
 * @property string $board_university
 * @property string $percentage
 * @property string $year_of_passing
 * @property string $stream
 *
 * @property User $user
 */
class User12 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_12';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['board_university', 'percentage', 'year_of_passing', 'stream'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'board_university' => 'Board University',
            'percentage' => 'Percentage',
            'year_of_passing' => 'Year Of Passing',
            'stream' => 'Stream',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
