<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_experience".
 *
 * @property string $id
 * @property string $user_id
 * @property string $company_name
 * @property string $months
 * @property string $year
 * @property string $department
 * @property string $designation
 * @property string $place
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserExperience extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['months','year'], 'safe'],
            [['company_name', 'designation','department','string'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_name' => 'Company Name',
            'worked_months' => 'Worked Months',
            'designation' => 'Designation',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
