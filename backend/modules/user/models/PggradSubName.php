<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "pggrad_sub_name".
 *
 * @property string $id
 * @property string $pg_id
 * @property string $name
 *
 * @property PggradSubject $pg
 */
class PggradSubName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pggrad_sub_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pg_id'], 'required'],
            [['pg_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['pg_id'], 'exist', 'skipOnError' => true, 'targetClass' => PggradSubject::className(), 'targetAttribute' => ['pg_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pg_id' => 'Pg ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPg()
    {
        return $this->hasOne(PggradSubject::className(), ['id' => 'pg_id']);
    }
}
