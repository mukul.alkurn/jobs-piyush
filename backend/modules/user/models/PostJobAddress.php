<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "post_job_address".
 *
 * @property string $post_job_id
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $pincode
 * @property string $country
 * @property string $tehsil
 * @property string $district
 *
 * @property PostJobs $postJob
 */
class PostJobAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_job_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_job_id'], 'required'],
            [['post_job_id'], 'integer'],
            [['city','state','country','pincode','address','tehsil','district'],'required'],

            [['address', 'city', 'state', 'pincode', 'country'], 'string', 'max' => 255],
            [['post_job_id'], 'unique'],
            [['post_job_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostJobs::className(), 'targetAttribute' => ['post_job_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'post_job_id' => 'Post Job ID',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'pincode' => 'Pincode',
            'country' => 'Country',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostJob()
    {
        return $this->hasOne(PostJobs::className(), ['id' => 'post_job_id']);
    }
}
