<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_grad".
 *
 * @property string $user_id
 * @property string $branch
 * @property string $board_university
 * @property string $percentage
 * @property string $year_of_passing
 * @property string $stream
 *
 * @property User $user
 */
class UserGrad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_grad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['branch', 'board_university', 'percentage', 'year_of_passing','stream'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'branch' => 'Branch',
            'board_university' => 'Board University',
            'percentage' => 'Percentage',
            'year_of_passing' => 'Year Of Passing',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
