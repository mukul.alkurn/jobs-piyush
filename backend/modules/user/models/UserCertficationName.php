<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_certfication_name".
 *
 * @property string $id
 * @property string $user_id
 * @property string $certificate_name
 * @property string $training_institute
 * @property string $location
 * @property string $year
 *
 * @property User $user
 */
class UserCertficationName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_certfication_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['certificate_name'], 'string', 'max' => 555],
            [['training_institute'], 'string', 'max' => 1000],
            [['location'], 'string', 'max' => 255],
            [['year'], 'string', 'max' => 200],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'certificate_name' => 'Certificate Name',
            'training_institute' => 'Training Institute',
            'location' => 'Location',
            'year' => 'Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
