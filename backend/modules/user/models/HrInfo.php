<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "hr_info".
 *
 * @property string $id
 * @property string $user_id
 * @property string $full_name
 * @property string $gender
 * @property string $year_of_joining
 * @property string $designation
 * @property string $email
 * @property string $phone
 *
 * @property User $user
 */
class HrInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hr_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['full_name', 'gender', 'year_of_joining', 'designation', 'email', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'full_name' => 'Full Name',
            'gender' => 'Gender',
            'year_of_joining' => 'Year Of Joining',
            'designation' => 'Designation',
            'email' => 'Email',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
