<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_education".
 *
 * @property string $id
 * @property string $user_id
 * @property string $qualification
 * @property string $date
 * @property string $school_college
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserEducation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['qualification', 'school_college'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'qualification' => 'Qualification',
            'date' => 'Date',
            'school_college' => 'School College',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
