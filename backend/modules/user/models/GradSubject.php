<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "grad_subject".
 *
 * @property string $id
 * @property string $name
 *
 * @property GradSubName[] $gradSubNames
 */
class GradSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grad_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 555],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradSubNames()
    {
        return $this->hasMany(GradSubName::className(), ['grad_id' => 'id']);
    }
}
