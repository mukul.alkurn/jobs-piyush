<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "post_job_specailities".
 *
 * @property string $id
 * @property string $post_job_id
 * @property string $speciality_id
 *
 * @property PostJobs $postJob
 * @property JobSpeciality $speciality
 */
class PostJobSpecailities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_job_specailities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_job_id'], 'required'],
            [['post_job_id', 'speciality_id'], 'integer'],
            [['post_job_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostJobs::className(), 'targetAttribute' => ['post_job_id' => 'id']],
            [['speciality_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSpeciality::className(), 'targetAttribute' => ['speciality_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_job_id' => 'Post Job ID',
            'speciality_id' => 'Speciality ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostJob()
    {
        return $this->hasOne(PostJobs::className(), ['id' => 'post_job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeciality()
    {
        return $this->hasOne(JobSpeciality::className(), ['id' => 'speciality_id']);
    }
}
