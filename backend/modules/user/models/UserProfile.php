<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile
 * @property string $avatar
 * @property string $orgnization_name
 * @property string $certificate
 * @property string $resume
 * @property int $experience
 * @property int $salary
 * @property int $english_low
 * @property int $english_average
 * @property int $english_medium
 * @property int $fresher
 * @property int $english_good
 * @property int $english_vgood
 * @property string $title
 * @property int $age
 * @property int $education_level
 * @property User $user
 * @property string $about
 * @property string $gender
 * @property string $estd_since
 * @property string $team_size
 * @property string $business_type
 * @property string $company_name
 * @property string $marital_status
 * @property string $dob
 */
class UserProfile extends \yii\db\ActiveRecord
{
    public $role;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_profile_approved','english_low','english_average','english_medium','english_good','english_vgood','fresher'], 'integer'],
            [['first_name', 'last_name', 'mobile','gender'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            ['mobile','number'],
            [['marital_status','dob'],'string'],
            ['about','string'],
            [['first_name', 'last_name', 'avatar', 'designation', 'mobile', 'telephone', 'nationality', 'certificate', 'orgnization_name', 'salary', 'title', 'age', 'resume', 'estd_since', 'team_size', 'business_type', 'company_name'], 'string', 'max' => 255],
            [['education_level'], 'string', 'max' => 2555],
            ['certificate','file'],
            ['mobile','required'],
            ['mobile','unique'],
            ['resume', 'image', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2, 'maxFiles' => 1],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'mobile' => Yii::t('app', 'Mobile'),
            'experience' => 'Experience',
            'salary' => 'Salary',
            'title' => 'Profile Title',
            'age' => 'Age',
            'education_level' => 'Education Level',
           // 'title' => 'Education Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getName()
    {
            if(!empty($this->first_name) && !empty($this->last_name)) {
            return sprintf('%s %s', $this->first_name, $this->last_name);
        } else {
            return sprintf('%s', ucfirst($this->user->username));
        }
    }
}
