<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "pggrad_subject".
 *
 * @property string $id
 * @property string $name
 *
 * @property PggradSubName[] $pggradSubNames
 */
class PggradSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pggrad_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 555],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPggradSubNames()
    {
        return $this->hasMany(PggradSubName::className(), ['pg_id' => 'id']);
    }
}
