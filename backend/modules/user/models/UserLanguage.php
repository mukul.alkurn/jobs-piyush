<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_language".
 *
 * @property string $id
 * @property string $user_id
 * @property string $language
 *
 * @property User $user
 */
class UserLanguage extends \yii\db\ActiveRecord
{
    public  $english_low;
    public  $english_average;
    public  $english_medium;
    public  $english_good;
    public  $english_vgood;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_language';
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['language'], 'safe'],
            [['english_low','english_average','english_medium','english_good','english_vgood'],'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'language' => 'Language',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
