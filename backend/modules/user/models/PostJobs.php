<?php

namespace backend\modules\user\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "post_jobs".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $posted_by
 * @property string $deadline
 * @property string $experience
 * @property string $industry
 * @property string $category
 * @property double $offered_salary
 * @property int $created_at
 * @property int $updated_at
 * @property string $hr_id
 *
 * @property PostJobAddress $postJobAddress
 * @property PostJobSpecailities[] $postJobSpecailities
 * @property User $postedBy
 * @property Industry $industry0
 * @property Experience $experience0
 * @property JobCategories $category0
 * @property ApplyJob[] $applyJobs

 */
class PostJobs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $specialities ;

    public static function tableName()
    {
        return 'post_jobs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['posted_by','title','specialities','experience','industry','deadline','category'], 'required'],
            [['posted_by','hr_id', 'job_type', 'experience', 'industry', 'created_at', 'updated_at'], 'integer'],
            [['deadline'], 'safe'],
            [['offered_salary','category'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['posted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['posted_by' => 'id']],
            [['job_type'], 'exist', 'skipOnError' => true, 'targetClass' => UserJobCategory::className(), 'targetAttribute' => ['job_type' => 'id']],
            [['industry'], 'exist', 'skipOnError' => true, 'targetClass' => Industry::className(), 'targetAttribute' => ['industry' => 'id']],
            [['experience'], 'exist', 'skipOnError' => true, 'targetClass' => Experience::className(), 'targetAttribute' => ['experience' => 'id']],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => JobCategories::className(), 'targetAttribute' => ['category' => 'id']],
            [['hr_id'], 'exist', 'skipOnError' => true, 'targetClass' => HrInfo::className(), 'targetAttribute' => ['hr_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'posted_by' => 'Posted By',
            'deadline' => 'Deadline',
            'job_type' => 'Job Type',
            'experience' => 'Experience',
            'industry' => 'Industry',
            'offered_salary' => 'Offered Salary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostJobAddress()
    {
        return $this->hasOne(PostJobAddress::className(), ['post_job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostJobSpecailities()
    {
        return $this->hasMany(PostJobSpecailities::className(), ['post_job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'posted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobType()
    {
        return $this->hasOne(UserJobCategory::className(), ['id' => 'job_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry0()
    {
        return $this->hasOne(Industry::className(), ['id' => 'industry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperience0()
    {
        return $this->hasOne(Experience::className(), ['id' => 'experience']);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ]; // TODO: Change the autogenerated stub
    }

    public function getCategory0()
    {

        return $this->hasOne(JobCategories::className(), ['id' => 'category']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplyJobs()
    {
        return $this->hasMany(ApplyJob::className(), ['post_id' => 'id']);
    }

    public function getJobs(){
        $result = new ActiveDataProvider([
            'query' =>$this->getApplyJobs()
        ]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHr()
    {
        return $this->hasOne(HrInfo::className(), ['id' => 'hr_id']);
    }
}
