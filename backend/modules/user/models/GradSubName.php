<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "grad_sub_name".
 *
 * @property string $id
 * @property string $grad_id
 * @property string $name
 *
 * @property GradSubject $grad
 */
class GradSubName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grad_sub_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grad_id'], 'required'],
            [['grad_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['grad_id'], 'exist', 'skipOnError' => true, 'targetClass' => GradSubject::className(), 'targetAttribute' => ['grad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grad_id' => 'Grad ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrad()
    {
        return $this->hasOne(GradSubject::className(), ['id' => 'grad_id']);
    }
}
