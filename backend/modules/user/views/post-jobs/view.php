<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\JobSpeciality */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Job Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cancellation-policy-view">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <div class="pull-right">
                <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> ' . Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-xs']) ?>
                <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> ' . Yii::t('app', 'View Applied '), ['view-applied','id'=>$model->id], ['class' => 'btn btn-info btn-xs']) ?>
                <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-xs',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'description:html',
                    [
                        'label' => 'Company Name',
                        'format'=>'raw',
                        'value' => function ($model) {

                            return Html::a($model->postedBy->userProfile->company_name ??$model->postedBy->email, ['/user/default/view-company', 'id' => $model->postedBy->id]);
                        }

                    ],
                    'deadline:date',
                    [
                        'label' => 'Category Name',
                        'attribute' => 'category0.name'
                    ],
                    [
                        'label' => 'Experience',
                        'attribute' => 'experience0.name'
                    ],
                    [
                        'label' => 'Industry Type',
                        'attribute' => 'industry0.name'
                    ],

                    [
                        'label' => 'Job Specialities',
                        'value' => function ($model) {
                            return implode(',', \yii\helpers\ArrayHelper::getColumn($model->postJobSpecailities, function ($model) {
                                return $model->speciality->name;
                            }));
                        }
                    ],
                    [
                        'label' => 'Applied Till',
                        'attribute' => 'Jobs.count'
                    ],


                    'offered_salary',
                    'created_at:date',
                    'updated_at:date',
                ]
            ]); ?>

        </div>
    </div>

</div>
