<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Job Posted';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-booking-index">
    <div class="box box-primary color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '<div class="text-right">{summary}</div>{items}<div class="text-center">{pager}</div>',
                'summaryOptions' => [
                    'tag' => 'p'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'title',
                    //'description:ntext',
                    [
                        'label' => 'Company Name',
                        'format'=>'raw',
                        'value' => function ($model) {

                            return Html::a($model->postedBy->userProfile->company_name ??$model->postedBy->email, ['/user/default/view-company', 'id' => $model->postedBy->id]);
                        }

                    ],

                    'deadline:date',
                    'category0.name',
                    //'job_type',
                    //'experience',
                    //'industry',
                    'offered_salary',
                    'created_at:date',
                    //'updated_at',

                    [
                        'template' => '{view} {delete}',
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', $url, ['class' => 'btn btn-primary btn-xs', 'title' => 'View']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', $url, ['class' => 'btn btn-danger btn-xs', 'title' => 'Delete', 'data-method' => 'POST', 'data-confirm' => 'Are you sure you want to delete?']);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>