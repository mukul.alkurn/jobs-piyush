<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View Applied job';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-booking-index">
    <div class="box box-primary color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $model,
                'layout' => '<div class="text-right">{summary}</div>{items}<div class="text-center">{pager}</div>',
                'summaryOptions' => [
                    'tag' => 'p'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'post.title',
                    [
                        'label' => ' Applier Name',
                        'format'=>'raw',
                        'value' => function ($model) {

                            return Html::a($model->user->userProfile->getName(), ['/user/default/view', 'id' => $model->user->id]);
                        }

                    ],
                    'messsage:text',
                    'created_at:date',
                    //'updated_at',

                    [
                        'template' =>'{view}',
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', $url, ['class' => 'btn btn-primary btn-xs', 'title' => 'View']);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>