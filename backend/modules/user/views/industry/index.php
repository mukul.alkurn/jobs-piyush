<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Industry';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-booking-index">
    <div class="box box-primary color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="pull-right">
                <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add New Industry', ['create'], ['class' => 'btn btn-success btn-xs']) ?>
            </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '<div class="text-right">{summary}</div>{items}<div class="text-center">{pager}</div>',
                'summaryOptions' => [
                    'tag' => 'p'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'name',
                    'slug',
                    'created_at:date',
                    // 'updated_at',

                    [
                        'template' =>'{view}{delete}',
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', $url, ['class' => 'btn btn-primary btn-xs', 'title' => 'View']);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', $url, ['class' => 'btn btn-info btn-xs', 'title' => 'Update']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', $url, ['class' => 'btn btn-danger btn-xs', 'title' => 'Delete', 'data-method' => 'POST', 'data-confirm' => 'Are you sure you want to delete?']);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>