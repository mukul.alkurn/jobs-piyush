<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Experience */

$this->title = 'Create Qualification';
$this->params['breadcrumbs'][] = ['label' => 'Experience', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-booking-create">
    <div class="box box-primary color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>