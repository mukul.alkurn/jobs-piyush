<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use alkurn\easythumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */

$this->title = $model->userProfile->getName();
$this->params['breadcrumbs'][] = ['label' => 'User Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body box-profile">
                    <hr>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Address Line',
                                'attribute' => 'userAddress.address_line'
                            ],
                            [
                                'label' => 'City',
                                'attribute' => 'userAddress.city',
                                'value' => function ($model) {
                                    return isset($model->userAddress->city) ? $model->userAddress->city : "-";
                                },
                            ],
                            [
                                'label' => 'State',
                                'attribute' => 'userAddress.state_id',
                                'value' => function ($model) {
                                    return isset($model->userAddress->state) ? $model->userAddress->state : "-";
                                },
                            ],
                            [
                                'label' => 'Country',
                                'attribute' => 'userAddress.country_id',
                                'value' => function ($model) {
                                    return isset($model->userAddress->country) ? $model->userAddress->country : "-";
                                },
                            ],
                            [
                                'label' => 'Zip Code',
                                'attribute' => 'userAddress.postal_code',
                                'value' => function ($model) {
                                    return $model->userAddress->postal_code;
                                },
                            ],
                        ],
                    ]) ?>
                </div>


                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Basic Details</h3>
                    <div class="pull-right">
                        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> List', ['index'], ['class' => 'btn btn-info btn-xs']) ?>
                        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> ' . Yii::t('app', 'View Posted Jobs '), ['/user/post-jobs','id'=>$model->id], ['class' => 'btn btn-info btn-xs']) ?>

                        <?php if (!$model->hasRole('admin')) { ?>
                            <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i> Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-xs',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $model,
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '-'
                        ],
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            [
                                'label' => 'Roles',
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    return implode(', ', $model->getAssignedRoles());
                                },
                            ],
                            [
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    return $model->getStatus();
                                }
                            ],

                            [
                                'label' => 'Mobile',
                                'attribute' => 'userProfile.mobile'
                            ],
                            [
                                'label' => 'Profile Title',
                                'attribute' => 'userProfile.title'
                            ],
                            [
                                'label' => 'Profile About',
                                'format' => "html",
                                'attribute' => 'userProfile.about'
                            ],
                            [
                                'label' => 'Job Posted',
                                'attribute' => 'Jobs.count'
                            ],
                            'userProfile.company_name',
                            'userProfile.estd_since',
                            'userProfile.team_size',
                            'userProfile.business_type',

                            [
                                'label' => 'Certificate',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->userProfile->certificate == null) return false;
                                    return Html::a('Download', '/uploads/' . $model->userProfile->certificate, ['download' => true]);
                                }
                            ],
                            [
                                'label' => 'Joined On',
                                'attribute' => 'created_at',
                                'format' => ['date']
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

        </div>

    </div>

</div>
