<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use mdm\admin\models\searchs\AuthItem;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */
/* @var $modelProfile backend\modules\user\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
$authManager = Yii::$app->getAuthManager();
$roles = ArrayHelper::map($authManager->getRoles(), 'name', 'name');
unset($roles['guest']);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB9CdZk3plxvoOJQEt_FT4ZOZ86rFRxTxw&libraries=places');
$status = \backend\modules\user\models\UserStatus::$list;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (!$model->isNewRecord) { ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?php } ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelProfile, 'mobile')->textInput(['disabled'=> $model->isNewRecord? false :true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelProfile, 'mobile')->textInput()->label('Mobile') ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($modelProfile, 'first_name')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelProfile, 'last_name')->textInput() ?>

        </div>

    </div>


    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'status')->dropDownList($status, ['prompt' => 'Select Status']) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add New User' : 'Update User', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

