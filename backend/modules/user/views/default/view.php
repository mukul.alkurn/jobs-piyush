<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use alkurn\easythumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */

$this->title = $model->userProfile->getName();
$this->params['breadcrumbs'][] = ['label' => 'User Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body box-profile">
                    <hr>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Address Line',
                                'attribute' => 'userAddress.address_line'
                            ],
                            [
                                'label' => 'City',
                                'attribute' => 'userAddress.city',
                                'value' => function ($model) {
                                    return isset($model->userAddress->city) ? $model->userAddress->city : "-";
                                },
                            ],
                            [
                                'label' => 'State',
                                'attribute' => 'userAddress.state_id',
                                'value' => function ($model) {
                                    return isset($model->userAddress->state) ? $model->userAddress->state : "-";
                                },
                            ],
                            [
                                'label' => 'Country',
                                'attribute' => 'userAddress.country_id',
                                'value' => function ($model) {
                                    return isset($model->userAddress->country) ? $model->userAddress->country : "-";
                                },
                            ],
                            [
                                'label' => 'Zip Code',
                                'attribute' => 'userAddress.postal_code',
                                'value' => function ($model) {
                                    return $model->userAddress->postal_code;
                                },
                            ],
                        ],
                    ]) ?>
                </div>

                <div class="box-body box-profile">
                    <hr>
                    <p>Certifications </p>
                    <?php
                            if(isset($model->userCertifications)){
                                $i =1;
                                foreach ($model->userCertifications as $key =>$value){
                                    echo Html::a('Certificate-'.$i,'/uploads/'.$value->file,['download'=>true]). "<br>";
                                    $i++;
                                }
                            }
                    ?>
                </div>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Basic Details</h3>
                    <div class="pull-right">
                        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> List', ['index'], ['class' => 'btn btn-info btn-xs']) ?>
                        <?php if (!$model->hasRole('admin')) { ?>
                            <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i> Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-xs',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $model,
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '-'
                        ],
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            [
                                'label' => 'Roles',
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    return implode(', ', $model->getAssignedRoles());
                                },
                            ],
                            [
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    return $model->getStatus();
                                }
                            ],

                            [
                                'label' => 'Mobile',
                                'attribute' => 'userProfile.mobile'
                            ],
                            [
                                'label' => 'Profile Title',
                                'attribute' => 'userProfile.title'
                            ],
                            [
                                'label' => 'Profile About',
                                'format' => "html",
                                'attribute' => 'userProfile.about'
                            ],
                            'userProfile.experience',
                            'userProfile.age',
                            'userProfile.education_level',
                            'userProfile.salary',
                            [
                                'label' => 'Job Category',
                                'value' => function ($model) {
                                    return implode(',', \yii\helpers\ArrayHelper::getColumn($model->userJobCategories, function ($model) {
                                        return $model->category->name;
                                    }));
                                }
                            ],

                            [
                                'label' => 'Resume',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->userProfile->resume == null) return false;
                                    return Html::a('Download', '/uploads/' . $model->userProfile->resume, ['download' => true]);
                                }
                            ],
                            [
                                'label' => 'Joined On',
                                'attribute' => 'created_at',
                                'format' => ['date']
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Education</h3>

                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $model->getEducation(),
                        'layout' => '<div class="text-right">{summary}</div>{items}<div class="text-center">{pager}</div>',
                        'summaryOptions' => [
                            'tag' => 'p'
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'qualification',
                            'date',
                            'school_college'

                        ],
                    ]); ?>
                </div>

            </div>



            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Experience</h3>
                </div>

                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $model->getExperience(),
                    'layout' => '<div class="text-right">{summary}</div>{items}<div class="text-center">{pager}</div>',
                    'summaryOptions' => [
                        'tag' => 'p'
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'company_name',
                        'designation',
                        'worked_months'
                        // 'updated_at',
                    ],
                ]); ?>
            </div>
        </div>

    </div>

</div>
